package vn.co.brycenvn.weather.common.managed;

import android.view.View;

public class ManagedComponent {
    private String componentClass;
    private String group;
    private String componentName;
    private Object data;
    private View uiComponent;

    /**
     * @param componentClass
     * @param group
     * @param componentName
     * @param uiComponent
     */
    public ManagedComponent(String componentClass, String group, String componentName, Object data, View uiComponent) {
        super();
        this.componentClass = componentClass;
        this.group = group;
        this.componentName = componentName;
        this.data = data;
        this.uiComponent = uiComponent;
    }


    /**
     * @param componentClass
     * @param data
     * @param uiComponent
     */
    public ManagedComponent(String componentClass, Object data, View uiComponent) {
        super();
        this.componentClass = componentClass;
        this.data = data;
        this.uiComponent = uiComponent;
        this.componentName = "";
        this.group = "";
    }


    public String getComponentClass() {
        return componentClass;
    }

    public void setComponentClass(String componentClass) {
        this.componentClass = componentClass;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public View getUiComponent() {
        return uiComponent;
    }

    public void setUiComponent(View uiComponent) {
        this.uiComponent = uiComponent;
    }

    public Object getData() {
        return data;
    }

}
