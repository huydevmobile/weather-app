package vn.co.brycenvn.weather.common.resourcemng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vn.co.brycenvn.weather.ForecastApplication;
import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.common.resourcemng.dto.MessageDto;
import vn.co.brycenvn.weather.common.resourcemng.dto.ResourceDto;
import vn.co.brycenvn.weather.common.resourcemng.dto.TextDto;
import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.service.CallbackListener;

/**
 * Lớp truy xuất dữ liệu text và message từ DB thông qua service
 *
 * @author q_huy
 */
public class FreshResource implements AppResource {
    public static Map<String, TextDto> mapText = new HashMap<String, TextDto>();
    public static Map<String, MessageDto> mapMessage = new HashMap<String, MessageDto>();

    public static String errorInternet = "";
    public static Map<String, TextDto> mapTextDefault = new HashMap<String, TextDto>();
    public static Map<String, MessageDto> mapMessageDefault = new HashMap<String, MessageDto>();

    private CallbackListener<Object> loadingCallback;

    FreshResource() {
        mapText = new HashMap<String, TextDto>();
        mapMessage = new HashMap<String, MessageDto>();
        mapTextDefault = new HashMap<String, TextDto>();
        mapMessageDefault = new HashMap<String, MessageDto>();
    }

    @Override
    public String getText(String screenId, int seqNo) {
        String key = screenId + "_" + seqNo;
        if (mapText.containsKey(key)) {
            return mapText.get(key).FORMITEMNM;
            // return mapMessage.get(key).MSG;
        }
        return FHConst.EMPTY_STRING;
    }

    @Override
    public String getMessage(String msgCd) {
        if (mapMessage.containsKey(msgCd)) {
            return mapMessage.get(msgCd).MSG;
        }
        return FHConst.EMPTY_STRING;
    }

    /**
     * Lấy dữ liệu text bằng cách gọi webservice
     *
     * @param id
     */
    @Override
    public void load(String id) {
    }

    @Override
    public String getMessageKbn(String msgCd) {
        if (mapMessage.containsKey(msgCd)) {
            return mapMessage.get(msgCd).MSGKBN;
        }
        return FHConst.EMPTY_STRING;
    }

    @Override
    public boolean isAvailable() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public List<ResourceDto> getTexts() {
        return new ArrayList<ResourceDto>(mapText.values());
    }

    @Override
    public List<ResourceDto> getMessages() {
        return new ArrayList<ResourceDto>(mapMessage.values());
    }

    void setLoadingCallback(CallbackListener<Object> callback) {
        this.loadingCallback = callback;
    }
}
