package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;
import vn.co.brycenvn.weather.service.dto.common.AccessInfoDto;

public class AccessInfoEntity implements WmsEntity {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String userCd;
    private String cstmCd;
    private String brnchCd;
    private String langCd;
    private String token;


    public static AccessInfoEntity fromDto(AccessInfoDto dto)
    {
        AccessInfoEntity entity = new AccessInfoEntity();
        entity.brnchCd = dto.BRNCHCD;
        entity.cstmCd = dto.CSTMCD;
        entity.userCd = dto.USRCD;
        entity.token = dto.TOKEN;
        entity.langCd=dto.LANG;
        return entity;
    }

    public String getUserCd() {
        return userCd;
    }

    public void setUserCd(String userCd) {
        this.userCd = userCd;
    }

    public String getCstmCd() {
        return cstmCd;
    }

    public void setCstmCd(String cstmCd) {
        this.cstmCd = cstmCd;
    }

    public String getBrnchCd() {
        return brnchCd;
    }

    public void setBrnchCd(String brnchCd) {
        this.brnchCd = brnchCd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLangCd() { return langCd; }

    public void setLangCd(String langCd) { this.langCd = langCd; }

    public AccessInfoDto toDto()
    {
        AccessInfoDto dto = new AccessInfoDto();
        dto.USRCD = this.userCd;
        dto.CSTMCD = this.cstmCd;
        dto.BRNCHCD = this.brnchCd;
        dto.TOKEN = this.token;
        dto.LANG =this.langCd;
        return dto;
    }
}
