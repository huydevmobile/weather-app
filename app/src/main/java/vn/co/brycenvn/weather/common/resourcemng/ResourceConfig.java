package vn.co.brycenvn.weather.common.resourcemng;

public class ResourceConfig {
    private String locaLocation;
    private String remoteLocation;

    public String getLocaLocation() {
        return locaLocation;
    }

    public void setLocaLocation(String locaLocation) {
        this.locaLocation = locaLocation;
    }

    public String getRemoteLocation() {
        return remoteLocation;
    }

    public void setRemoteLocation(String remoteLocation) {
        this.remoteLocation = remoteLocation;
    }


}
