package vn.co.brycenvn.weather.common.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

//import com.crashlytics.android.Crashlytics;

import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.common.constant.MessageCode;
import vn.co.brycenvn.weather.common.managed.ManagedComponent;
import vn.co.brycenvn.weather.common.resourcemng.ResourceManager;
import vn.co.brycenvn.weather.entity.DataBundle;
import vn.co.brycenvn.weather.entity.cm.SystemEntity;

public class WmsUtil {

    public static int DEVICETYPE = 2;

    public static boolean isNumber(String text) {
        return Pattern.matches("^[0-9]+$", text);
    }

    public static boolean isLimitNumber(String text) {
        return text.length() <= 9;
    }

    public static String dateToString(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat(FHConst.STR_FORMAT_DFLTDATE);
        return sdf.format(d);
    }

    public static String dateToSqlString(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat(FHConst.STR_FORMAT_MYSQLDATE);
        return sdf.format(d);
    }

    public static Date stringToDate(String strDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(FHConst.STR_FORMAT_DFLTDATE);
        try {
            return new java.sql.Date(sdf.parse(strDate).getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Chuy盻ハ chu盻擁 ngﾃ�y thﾃ｡ng t盻ｫ d蘯｡ng yyyyMMdd sang d蘯｡ng yyyy-MM-dd
     *
     * @param strDate Chu盻擁 ngﾃ�y thﾃ｡ng ﾄ黛ｻ杵h d蘯｡ng yyyyMMdd
     * @return
     */
    public static String convertToSqlFormat(String strDate) {
        if (strDate.trim().equals(FHConst.EMPTY_STRING)) {
            return FHConst.EMPTY_STRING;
        }
        Date d = stringToDate(strDate);
        return dateToSqlString(d);
    }

    /**
     * Chuy盻ハ chu盻擁 ngﾃ�y thﾃ｡ng t盻ｫ d蘯｡ng d蘯｡ng yyyy-MM-dd sang d蘯｡ng yyyyMMdd
     *
     * @param strDate Chu盻擁 ngﾃ�y thﾃ｡ng ﾄ黛ｻ杵h d蘯｡ng yyyy-MM-dd
     * @return
     */
    public static String convertToAppFormat(String strDate) {
        if (strDate.trim().equals(FHConst.EMPTY_STRING)) {
            return FHConst.EMPTY_STRING;
        }
        return strDate.replace("-", "");
    }

    /**
     * FHCommon_BarCodeChange
     *
     * @param entryValue
     * @return
     */
    public static String barcodeChange(String entryValue) {
        String separatorStr;
        try {
            // 隴伜挨逕ｨ譁�蟄励ｒ蜿門ｾ�
            separatorStr = "a";
            // 隴伜挨逕ｨ譁�蟄励′荳｡遶ｯ縺ｫ蜷ｫ縺ｾ繧後※縺�繧九°繝√ぉ繝�繧ｯ
            if (entryValue.indexOf(separatorStr) == 0
                    && entryValue.lastIndexOf(separatorStr) == entryValue.length() - 1) {
                if (entryValue.indexOf(separatorStr) != entryValue.lastIndexOf(separatorStr)) {
                    // 譁�蟄励�ｮ菴咲ｽｮ縺悟酔縺倥〒縺ｪ縺�蝣ｴ蜷�(荳�蛟九＠縺句性縺ｾ繧後※縺�縺ｪ縺�)縺ｯ螟画鋤
                    // TODO check substring function
                    entryValue = entryValue.substring(entryValue.indexOf(separatorStr) + 2,
                            entryValue.lastIndexOf(separatorStr) - entryValue.indexOf(separatorStr) - 1);
                }
            }
        } catch (Exception ex) {
            throw ex;
        }
        return entryValue;
    }

    public static int getCodeData(SystemEntity sysE, String code, ArrayList<String> arrData) {
        char delim = '*';
        if (TextUtils.isEmpty(sysE.getSysKbn50())) {
            delim = FHConst.CNST_SYS_DELIM;
        } else {
            delim = sysE.getSysKbn50().charAt(0);
        }
        if (TextUtils.isEmpty(code)) {
            return 0;
        }
        String[] arrSplit = code.split(String.valueOf(delim));
        for (String str : arrSplit) {
            arrData.add(str);
        }
        return arrData.size();
    }


    public static String getItemCodeFromSerial(SystemEntity sysE, String serialNumber) {
        int codeLength = serialNumber.length();
        String str = serialNumber;
        if (sysE.getSysFlg40().equals("1")) {
            if (serialNumber.length()>=Integer.valueOf(sysE.getSysKbn32()))
            str = serialNumber.substring(0, Integer.valueOf(sysE.getSysKbn32()));
        } else if (sysE.getSysFlg40().equals("3")) {
            str = serialNumber;
        }
        else{
            str = serialNumber;
        }

        return str;
    }
    public static String getXJcodeFromSerial(SystemEntity sysE, String serialNumber) {
        String str = serialNumber;
        if (sysE.getSysFlg40().equals("1")) {
            if (serialNumber.contains("--")){
                String[] rs=serialNumber.split("--");
                if (rs.length==2){
                    str=rs[1]; //lay string sau --
                    return str;
                }

            }
        }
        return  str;
    }

    public static String getQuantityFromSerialTonner(SystemEntity sysE, String serialNumber) {
        String str;
        int codeLength = serialNumber.length();
        if (codeLength >= 12) {
            str = serialNumber.substring(10, 12);
        } else {
            sysE.setCheckLengthCut(false);
            return "";
        }
        return str.trim();
    }

    public static String getCodeDataRicolCheckIncreaseQty(SystemEntity sysE, String code) {
        char delim1 = '*';
        char delim2 = ' ';
        int numberCut = 7;
        ArrayList<String> arrDataFirst = new ArrayList<>();
        ArrayList<String> strRet = new ArrayList<>();
        if (TextUtils.isEmpty(code)) {
            return "";
        }
        String[] arrSplit = code.split(String.valueOf(delim1));
        for (String str : arrSplit) {
            arrDataFirst.add(str);
        }
        if (arrDataFirst.size() == 1) {
            strRet.add(arrDataFirst.get(0).substring(0, 6));
        } else {
            for (int i = 0; i < arrDataFirst.size() - 1; i++) {
                arrDataFirst.add(arrDataFirst.get(i).substring(0, 6));
            }
        }

        return convertArrToString(arrDataFirst);
    }


    public static String convertArrToString(ArrayList<String> listSerial) {
        String strFinal;
        StringBuilder builder = new StringBuilder();
        for (String item : listSerial) {
            String string = item + ",";
            builder.append(string);
        }
        strFinal = removeCommaFinal(builder.toString());
        return strFinal;
    }

    public static String removeCommaFinal(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ',') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }


    public static String getMessage(CommonEntity cmnE) {
        String msg = ResourceManager.getMessage(cmnE.getMsgCode());

        if (cmnE.getSubMsgCode1() != null && !cmnE.getSubMsgCode1().equals("")) {
            msg = msg.replace("%1", ResourceManager.getMessage(cmnE.getSubMsgCode1()));
        }
        if (cmnE.getSubMsgCode2() != null && !cmnE.getSubMsgCode2().equals("")) {
            msg = msg.replace("%2", ResourceManager.getMessage(cmnE.getSubMsgCode2()));
        }
        if (cmnE.getSubMsgCode3() != null && !cmnE.getSubMsgCode3().equals("")) {
            msg = msg.replace("%3", ResourceManager.getMessage(cmnE.getSubMsgCode3()));
        }
        if (cmnE.getSubMsgCode4() != null && !cmnE.getSubMsgCode4().equals("")) {
            msg = msg.replace("%4", ResourceManager.getMessage(cmnE.getSubMsgCode4()));
        }

        return msg;
    }


    /**
     * Get text for form control
     *
     * @param screenId Name of screen
     * @param baseNo   Base number of control (tag value)
     * @param state    State of control
     * @return Text
     * @see ManagedComponent
     */
    public static String getText(String screenId, int baseNo, int state) {
        return ResourceManager.getText(screenId, baseNo + state);
    }

    public static int LenB(String strInputValue) {
        // TODO Auto-generated method stub
        byte[] bt = strInputValue.getBytes(Charset.forName("UTF-8"));
        return bt.length;
    }

    @SuppressLint("SimpleDateFormat")
    public static Boolean IsDate(String argTarget) {
        String strMyString;
        Boolean iReturn = true;
        SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
        ft.setLenient(false);
        try {
            if (argTarget.length() == 0) {
                return true;
            }
            if (argTarget.length() == 8) {
                strMyString = argTarget.substring(0, 4) + "/" + argTarget.substring(4, 6) + "/"
                        + argTarget.substring(6, 8);
            } else {
                strMyString = argTarget;
            }
            ft.parse(strMyString.trim());
        } catch (Exception ex) {
            iReturn = false;
        }
        return iReturn;
    }

    public static void savePrefString(Context context, String key, String saveContent) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, saveContent);
        editor.apply();
    }

    public static String getPrefString(Context context, String key, String def) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        String strResult = preferences.getString(key, def);
        return strResult;
    }

    public static void deletePrefString(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().clear().commit();
    }

    public static void hideSoftKeyboard(Activity activity) {
        try {
            // Check if no view has focus:
            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showLog(Exception ex) {

    }

    public static void showLog2(Exception ex, DataBundle dataBundle) {
        showLog(ex);
        dataBundle.setExtraVar(FHConst.RETURN_CODE, 1);
        dataBundle.getCommonEntity().setMsgCode(MessageCode.MS000001);

    }

    public static void Vibrator(Context context) {
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(800, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            //deprecated in API 26
            v.vibrate(800);
        }
    }

    public static void makeSoundNotiftion(Context context) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(context.getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean equalLists(List<String> one, List<String> two) {
        if (one == null && two == null) {
            return false;
        }
        Set<String> intersection = new HashSet<String>(one);
        intersection.retainAll(two);

        return intersection.size() > 0;
    }

    public static int dpToPx(Context context, int dp) {
        float density = context.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }

    public static String getCurrentDate() {
        String currentDate;
        currentDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());
        return currentDate;
    }
    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }
    public static boolean isToday(Date date){
        Calendar today = Calendar.getInstance();
        Calendar specifiedDate  = Calendar.getInstance();
        specifiedDate.setTime(date);

        return today.get(Calendar.DAY_OF_MONTH) == specifiedDate.get(Calendar.DAY_OF_MONTH)
                &&  today.get(Calendar.MONTH) == specifiedDate.get(Calendar.MONTH)
                &&  today.get(Calendar.YEAR) == specifiedDate.get(Calendar.YEAR);
    }
    public static String getToday(Calendar calendar){
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        return year+"/"+month+"/"+date;
    }
    public static String capitalizeString(String str) {
        String retStr = str.toLowerCase();
        try {
            retStr = str.substring(0, 1).toUpperCase() + str.substring(1);
        }catch (Exception e){}
        return retStr;
    }
}
