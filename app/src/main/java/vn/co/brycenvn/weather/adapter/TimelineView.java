package vn.co.brycenvn.weather.adapter;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import vn.co.brycenvn.weather.activity.WeatherActivity;

public class TimelineView extends RecyclerView {
    private static final String TAG = "TimelineView";
    private TimelineAdapter adapter;
    private int monthTextColor, dateTextColor, dayTextColor, selectedColor, disabledColor;
//    private float monthTextSize, dateTextSize, dayTextSize;
    private int year, month, date;
    private WeatherActivity weatherActivity;
    @Override
    public boolean fling(int velocityX, int velocityY) {
        return super.fling(velocityX, velocityY);
    }
    private int firstposition;
    public boolean isScrollFinish;
    @Override
    public void onScrollStateChanged(int state) {
        super.onScrollStateChanged(state);
        if (!TimelineView.this.canScrollVertically(1) && state == RecyclerView.SCROLL_STATE_IDLE) {
            //reached end
            LinearLayoutManager manager= (LinearLayoutManager) TimelineView.this.getLayoutManager();
            assert manager != null;
            int visiblePosition = manager.findLastCompletelyVisibleItemPosition();
            firstposition = manager.findFirstCompletelyVisibleItemPosition();
            adapter.notifyDataSetChanged();
            int flag=adapter.flagScroll;
            if (firstposition<1){
//                layoutManager.scrollToPositionWithOffset(0,2);
                layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),0);
            }
            else
            if (firstposition>=2 && firstposition <=7){
//                layoutManager.scrollToPositionWithOffset(7,2);
                if (flag==1){
                    layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),13);
                }
                else{
                    layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),0);
                }
            }
            else
            if (firstposition<14){
//                layoutManager.scrollToPositionWithOffset(15,2);
                if (flag==1){
                    layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),21);
                }
                else{
//                    layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),13);
                    layoutManager.scrollToPositionWithOffset(7,2);
                }
            }
            else{
                if (flag==0 && firstposition <=14){
//                    layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),13);
                    layoutManager.scrollToPositionWithOffset(7,2);
                }
            }
            selectedNewViewHolderWhenScrollEnd(TimelineView.this,state);
            isScrollFinish=true;
        }
        if (!TimelineView.this.canScrollVertically(-1) && state == RecyclerView.SCROLL_STATE_IDLE) {
            //reached top
        }
        if(state == RecyclerView.SCROLL_STATE_DRAGGING){
            //scrolling
        }
    }
    public void changeSelectPosition(){
        int current_select_pos=adapter.getSelectedPosition();
        if (current_select_pos<7){
            if (firstposition<7){

            }
            else if (firstposition<14){
                adapter.setSelectedPosition(adapter.getSelectedPosition()+7);
            }
            else {
                adapter.setSelectedPosition(adapter.getSelectedPosition()+14);
            }
        }
        else
        if (current_select_pos <14){
            if (firstposition<7){
                adapter.setSelectedPosition(adapter.getSelectedPosition()-7);
            }
            else if (firstposition<14){

            }
            else {
                adapter.setSelectedPosition(adapter.getSelectedPosition()+7);
            }
        }
        else
        {
            if (firstposition<7){
                adapter.setSelectedPosition(adapter.getSelectedPosition()-14);
            }
            else if (firstposition<14){
                adapter.setSelectedPosition(adapter.getSelectedPosition()-7);
            }
            else {
            }
        }
    }

    @Override
    public void onScrolled(int dx, int dy) {
        super.onScrolled(dx, dy);
        isScrollFinish=false;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);


    }

    private void selectedNewViewHolderWhenScrollEnd(TimelineView timelineView,int state) {
        if (!TimelineView.this.canScrollVertically(1) && state == RecyclerView.SCROLL_STATE_IDLE) {
            LinearLayoutManager manager= (LinearLayoutManager) TimelineView.this.getLayoutManager();
            assert manager != null;
            int visiblePosition = manager.findLastCompletelyVisibleItemPosition();
            firstposition = manager.findFirstCompletelyVisibleItemPosition();
            firstposition = manager.findFirstCompletelyVisibleItemPosition();
            changeSelectPosition();
            TimelineAdapter.ViewHolder cur_viewHolder= (TimelineAdapter.ViewHolder)
                    findViewHolderForAdapterPosition(adapter.getSelectedPosition());
            if (cur_viewHolder==null){
                return;
            }
            adapter.highlightItem(cur_viewHolder.rootView,adapter.getSelectedPosition());
        }

    }

    public TimelineView(@NonNull Context context) {
        super(context);
        init();
    }

    public TimelineView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TimelineView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    LinearLayoutManager layoutManager;
    void init() {
        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        date = calendar.get(Calendar.DAY_OF_MONTH)-7;
        setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,
                false);
        setLayoutManager(layoutManager);
        adapter = new TimelineAdapter(this, -1);
        setAdapter(adapter);
        layoutManager.smoothScrollToPosition(this,new RecyclerView.State(),13);
    }

    public int getMonthTextColor() {
        return monthTextColor;
    }

    public void setMonthTextColor(int color) {
        this.monthTextColor = color;
    }

    public int getDateTextColor() {
        return dateTextColor;
    }

    public void setDateTextColor(int color) {
        this.dateTextColor = color;
    }

    public int getDayTextColor() {
        return dayTextColor;
    }

    public void setDayTextColor(int color) {
        this.dayTextColor = color;
    }

    public void setDisabledDateColor(int color) {
        this.disabledColor = color;
    }

    public int getDisabledDateColor() {
        return disabledColor;
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    public void setSelectedColor(int color) {
        this.selectedColor = color;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDate() {
        return date;
    }

    public void setOnDateSelectedListener(OnDateSelectedListener listener) {
        adapter.setDateSelectedListener(listener);
    }

    public void setInitialDate(int year, int month, int date) {
        this.year = year;
        this.month = month;
        this.date = date;
        invalidate();
    }

    /**
     * Calculates the date position and set the selected background on that date
     * @param activeDate active Date
     */
    public void setActiveDate(Calendar activeDate) {
        try {
            Date initialDate = new SimpleDateFormat("yyyy-MM-dd")
                    .parse(year + "-" + (month + 1) + "-" + this.date);
            long diff =  activeDate.getTime().getTime() - initialDate.getTime();
            int position = (int) (diff / (1000 * 60 * 60 * 24));
            adapter.setSelectedPosition(position);
            invalidate();
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public void deactivateDates(Date[] deactivatedDates) {
        adapter.disableDates(deactivatedDates);
    }

    public void setWeatherActivity(WeatherActivity weatherActivity) {
        this.weatherActivity = weatherActivity;
        adapter.setWeatherActivity(weatherActivity);
    }
}
