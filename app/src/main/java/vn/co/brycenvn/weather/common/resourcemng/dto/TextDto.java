package vn.co.brycenvn.weather.common.resourcemng.dto;


public class TextDto extends ResourceDto {
    public static final String DTO_ID = "shcmFormItemRows";
    public String SCRNID;
    public String SEQNO;
    public String FORMITEMNM;

    public TextDto() {

    }


    /**
     * @param sCRNID
     * @param sEQNO
     * @param fORMITEMNM
     */
    public TextDto(String sCRNID, String sEQNO, String fORMITEMNM) {
        super();
        SCRNID = sCRNID;
        SEQNO = sEQNO;
        FORMITEMNM = fORMITEMNM;
    }

    public TextDto(String[] arr) {
        if (arr.length > 0) {
            SCRNID = arr[0];
        }
        if (arr.length > 1) {
            SEQNO = arr[1];
        }
        if (arr.length > 2) {
            FORMITEMNM = arr[2];
        }
    }


    @Override
    public String toString() {
        return SCRNID + "\t" + SEQNO + "\t" + FORMITEMNM;
    }
}
