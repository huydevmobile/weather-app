package vn.co.brycenvn.weather.entity;

import java.util.HashMap;
import java.util.Map;

import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.entity.cm.StockEntity;

public abstract class DataBundle {
    public static int stateScreen = 1;
    public static int ret_code;
    protected CommonEntity commonEntity;
    protected StockEntity stockEntity;
    private Map<String, Object> mapStorage;
    public String exmess = "";

    public DataBundle(CommonEntity cmne) {
        this.commonEntity = cmne;
        this.mapStorage = new HashMap<String, Object>();
        stockEntity = new StockEntity();
    }

    public String getExMess() {
        return exmess;
    }

    public void setExMess(String subMsgCode1) {
        this.exmess = subMsgCode1;
    }

    public abstract String getScreenId();

    /**
     * Get common entity
     *
     * @return
     */
    public CommonEntity getCommonEntity() {
        return commonEntity;
    }

    /**
     * Lưu trữ giá trị biến tùy chọn vào data bundle
     *
     * @param key
     * @param value
     */
    public final void setExtraVar(String key, Object value) {
        if (key.equals(FHConst.RETURN_CODE)) {
            ret_code = (Integer) value;
        }
        this.mapStorage.put(key, value);
    }

    /**
     * Lấy giá trị biến tùy chọn ra từ data bundle
     *
     * @param type Kiểu của biến
     * @param key  khóa truy xuất
     * @return Null nếu key không tồn tại
     */
    public final <T extends Object> T getExtraVar(Class<T> type, String key) {
        if (this.mapStorage.containsKey(key)) {
            Object obj = this.mapStorage.get(key);
            return type.cast(obj);
        }
        return null;
    }

    /**
     * Get stock entity. Override this method to return stock entity instance
     *
     * @return Return null value as default.
     */
    public StockEntity getStockEntity() {
        return stockEntity;
    }


}
