package vn.co.brycenvn.weather.service.consum;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.co.brycenvn.weather.ForecastApplication;
import vn.co.brycenvn.weather.activity.WeatherActivity;
import vn.co.brycenvn.weather.common.constant.ErrorCode;
import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.common.util.WmsUtil;
import vn.co.brycenvn.weather.config.ConfigurationManager;
import vn.co.brycenvn.weather.entity.DataBundle;
import vn.co.brycenvn.weather.entity.WeatherDataBundle;
import vn.co.brycenvn.weather.localdb.AppDatabase;
import vn.co.brycenvn.weather.localdb.Item;
import vn.co.brycenvn.weather.localdb.dao.ItemDAO;
import vn.co.brycenvn.weather.service.AbstractServiceConsumer;
import vn.co.brycenvn.weather.service.Request;
import vn.co.brycenvn.weather.service.ServiceCall;
import vn.co.brycenvn.weather.service.StateCallbackListener;
import vn.co.brycenvn.weather.service.WmsService;
import vn.co.brycenvn.weather.service.dto.DateWeatherDto;

import static vn.co.brycenvn.weather.activity.BaseActivity.checkNetwork;

public class WeatherService extends AbstractServiceConsumer {
    private static final String API_GET_DATA = "api/location/1252431/";
    private WmsService serviceCaller = new WmsService(ConfigurationManager
            .getInstance().getConfig(FHConst.SERVICE_URL));
    private int sub=-1;
    public void getWheatherDataService(Activity context, final WeatherDataBundle bundle,
                           final StateCallbackListener<DataBundle> callback,boolean isLoading,int year, int month, int day,boolean online) {
        Request request;
        Calendar calendar = Calendar.getInstance();
        if (year==0){
            request = createRequest(API_GET_DATA+ WmsUtil.getToday(Calendar.getInstance()));
        }
        else{
            request = createRequest(API_GET_DATA+ year+"/"+month+"/"+day);
        }
        final ArrayList<DateWeatherDto> infoE = bundle
                .getDetailsEntity();
        StateCallbackListener<JSONArray> serviceCallback = new StateCallbackListener<JSONArray>() {

            @Override
            public void onSuccess(JSONArray data) throws Exception {
                try {
                    if ((data == null || data.length()==0) && online) {
                        bundle.setExtraVar(FHConst.RETURN_CODE,
                                ErrorCode.ERR_SERVICE);
                        callback.onFailure(bundle);
                        return;
                    }
                    if (!checkError(data)) {
                        return;
                    }
                    Gson gson = new Gson();
                    infoE.removeAll(infoE);
                    ItemDAO itemDAO = ((ForecastApplication) context.getApplication()).getDatabase().getItemDAO();
                    for (int i = 0; i < data.length(); i++) {
                        DateWeatherDto item = gson.fromJson(data.getJSONObject(i).toString(), DateWeatherDto.class);
                        infoE.add(item);
                        insertDbLocal(data, gson, itemDAO, i, day, month, year);
                    }
                    if (online) {
                        if (sub==-1){
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    int year = calendar.get(Calendar.YEAR);
                                    int month = calendar.get(Calendar.MONTH)+1;
                                    int date = calendar.get(Calendar.DAY_OF_MONTH);
                                    WeatherActivity weatherActivity=((WeatherActivity) context);
                                    weatherActivity.callGetData(year,month,date,false);
                                }
                            });
                        }
                        bundle.setExtraVar(FHConst.RETURN_CODE,
                                ErrorCode.SUCCESS);
                    }
                    else{
                        sub++;
                        if (sub<=21){
                            context.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    int year = calendar.get(Calendar.YEAR);
                                    int month = calendar.get(Calendar.MONTH)+1;
                                    int date = calendar.get(Calendar.DAY_OF_MONTH)-7+sub;
                                    WeatherActivity weatherActivity=((WeatherActivity) context);
                                    weatherActivity.callGetData(year,month,date,false);
                                }
                            });
                        }

                        bundle.setExtraVar(FHConst.RETURN_CODE,
                                ErrorCode.SUCCESS_OFFLINE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    WmsUtil.showLog(e);
                    onFailure(data);

                }
                callback.onSuccess(bundle);
            }

            @Override
            public void onFailure(JSONArray data) throws Exception {
                if (data != null) {
                    JSONObject jsonObject=data.getJSONObject(0);
                    if (jsonObject.getString("Error").equals("NoInternet")) {
                        bundle.setExtraVar(FHConst.RETURN_CODE, ErrorCode.FAIL_NETWORK);
                    }
                } else {
                    bundle.setExtraVar(FHConst.RETURN_CODE, ErrorCode.ERR_UNKNOW);

                }
                callback.onFailure(bundle);
            }
        };
        getServiceCaller().get(context, isLoading, request, serviceCallback,null,null,online);
    }

    private void insertDbLocal(JSONArray data, Gson gson, ItemDAO itemDAO, int i, int day, int month, int year) throws JSONException {
        Item itemlc = gson.fromJson(data.getJSONObject(i).toString(), Item.class);
        itemlc.setDay(day);
        itemlc.setMonth(month);
        itemlc.setYear(year);
        itemDAO.deleteItemByDate(day,month,year);
        itemDAO.insert(itemlc);
    }
    @Override
    protected WmsService getServiceCaller() {
        return this.serviceCaller;
    }
}
