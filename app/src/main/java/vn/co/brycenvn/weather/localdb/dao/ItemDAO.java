package vn.co.brycenvn.weather.localdb.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import vn.co.brycenvn.weather.localdb.Item;

@Dao
public interface ItemDAO {
    @Insert
    public void insert(Item... items);
    @Update
    public void update(Item... items);
    @Delete
    public void delete(Item item);

    @Query("SELECT * FROM items")
    public List<Item> getItems();

    @Query("SELECT * FROM items WHERE id = :id")
    public Item getItemById(Long id);

    @Query("SELECT * FROM items WHERE day = :day AND month = :month AND year = :year")
    public List<Item> getItemByDate(int day,int month,int year);
    @Query("DELETE FROM items WHERE day = :day AND month = :month AND year = :year")
    public void deleteItemByDate(int day,int month,int year);
}