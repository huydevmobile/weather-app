package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

/**
 * Created by q_huy on 2/22/2018.
 */

public class SizeEntity implements WmsEntity {
    private String sizeCode;
    private String sizeName;

    public SizeEntity(String sizeCode, String sizeName) {
        this.sizeCode = sizeCode;
        this.sizeName = sizeName;
    }

    public String getSizeCode() {
        return sizeCode;
    }

    public void setSizeCode(String sizeCode) {
        this.sizeCode = sizeCode;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }
}
