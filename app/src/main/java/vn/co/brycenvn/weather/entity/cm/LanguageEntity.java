package vn.co.brycenvn.weather.entity.cm;

/**
 * Created by q_huy on 9/7/2017.
 */

public class LanguageEntity {
    private String langCd;
    private String langNm;
    private String langRnm;


    public LanguageEntity() {
        super();
    }

    public LanguageEntity(String langCd, String langNm, String langRnm) {
        super();
        this.langCd = langCd;
        this.langNm = langNm;
        this.langRnm = langRnm;
    }
    public String getLangCode() {
        return langCd;
    }
}
