package vn.co.brycenvn.weather.entity;

import java.util.ArrayList;

import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.service.dto.DateWeatherDto;

public class WeatherDataBundle extends DataBundle{
    private ArrayList<DateWeatherDto> detailsEntity;
    private ArrayList<String> list_weather_state_abbr;
    private ArrayList<Float> list_temp;
    private ArrayList<String> list_weather_state_name;

    private ArrayList<Float> list_humidity;
    private ArrayList<Float> list_predictability;

    private static final String NAME = "Weather";
    public WeatherDataBundle(CommonEntity cmne) {
        super(cmne);
        detailsEntity = new ArrayList<>();
        list_weather_state_abbr =new ArrayList<>();
        list_temp =new ArrayList<>();
        list_weather_state_name =new ArrayList<>();
        list_humidity=new ArrayList<>();
        list_predictability=new ArrayList<>();
    }

    @Override
    public String getScreenId() {
         return NAME;
    }

    public ArrayList<DateWeatherDto> getDetailsEntity() {
        return detailsEntity;
    }

    public void setDetailsEntity(ArrayList<DateWeatherDto> detailsEntity) {
        this.detailsEntity = detailsEntity;
    }

    public ArrayList<String> getList_weather_state_abbr() {
        return list_weather_state_abbr;
    }

    public void setList_weather_state_abbr(ArrayList<String> list_weather_state_abbr) {
        this.list_weather_state_abbr = list_weather_state_abbr;
    }

    public ArrayList<Float> getList_temp() {
        return list_temp;
    }

    public void setList_temp(ArrayList<Float> list_temp) {
        this.list_temp = list_temp;
    }

    public ArrayList<String> getList_weather_state_name() {
        return list_weather_state_name;
    }

    public void setList_weather_state_name(ArrayList<String> list_weather_state_name) {
        this.list_weather_state_name = list_weather_state_name;
    }

    public ArrayList<Float> getList_humidity() {
        return list_humidity;
    }

    public void setList_humidity(ArrayList<Float> list_humidity) {
        this.list_humidity = list_humidity;
    }

    public ArrayList<Float> getList_predictability() {
        return list_predictability;
    }

    public void setList_predictability(ArrayList<Float> list_predictability) {
        this.list_predictability = list_predictability;
    }
}
