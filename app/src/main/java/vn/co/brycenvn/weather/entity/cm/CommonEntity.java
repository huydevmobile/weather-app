package vn.co.brycenvn.weather.entity.cm;

import java.util.ArrayList;
import java.util.List;

import vn.co.brycenvn.weather.entity.WmsEntity;

public class CommonEntity implements WmsEntity {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public List<MenuEntity> mainMenus = new ArrayList<MenuEntity>();
    public List<MenuEntity> subMenus = new ArrayList<MenuEntity>();
    public List<Tmt280DataEntity> tmt280Data = new ArrayList<>();
    private String pcId;
    private String customerCode;
    private String branchCode;
    private String userCode;
    private String entryValue;
    private String msgContent;
    private String msgCode;
    private String msgName;
    private String subMsgCode1;
    private String subMsgCode2;
    private String subMsgCode3;
    private String subMsgCode4;
    private boolean commonLotDispFlag;
    private int retWinNo;
    private SystemEntity systemEntity;
    private AccessInfoEntity accessInfo;
    private List<QualityEntity> qualities;
    private List<ReasonEntity> reasons;
    private List<ColorEntity> colors;
    private List<SizeEntity> size;
    private List<BranchFromEntity> branch;
    private QualityEntity defaultQuality;
    private String defaultLocationSys;
    private String defaultBranch;
    private String defaultLanguage;
    private String defaultCstm;
    // session info
    private String token;
    private ArrayList<String> strSubMenu;
    private ArrayList<String> strSubMenu2;

    public ArrayList<ColorEntity> getArrcolor() {
        return arrcolor;
    }

    public void setArrcolor(ArrayList<ColorEntity> arrcolor) {
        this.arrcolor = arrcolor;
    }

    public ArrayList<SizeEntity> getArrsizes() {
        return arrsizes;
    }

    public void setArrsizes(ArrayList<SizeEntity> arrsizes) {
        this.arrsizes = arrsizes;
    }

    private ArrayList<ColorEntity> arrcolor;
    private ArrayList<SizeEntity> arrsizes;


    //for Image index
    private ArrayList<Integer> indexImageSubMenu;
    public CommonEntity() {
        systemEntity = new SystemEntity();
        reasons= new ArrayList<>();
        qualities = new ArrayList<>();
        strSubMenu = new ArrayList<>();
        strSubMenu2 = new ArrayList<>();
        indexImageSubMenu= new ArrayList<>();
        colors= new ArrayList<>();
        size=new ArrayList<>();
    }

    public List<MenuEntity> getMainMenus() {
        return mainMenus;
    }

    public List<MenuEntity> getSubMenus() {
        return subMenus;
    }

    public ArrayList<String> getSubMenu() {
        return strSubMenu;
    }

    public ArrayList<String> getSubMenu2() {
        return strSubMenu2;
    }

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getEntryValue() {
        return entryValue;
    }

    public void setEntryValue(String entryValue) {
        this.entryValue = entryValue;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(String msgCode) {
        this.msgCode = msgCode;
    }

    public String getMsgName() {
        return msgName;
    }

    public void setMsgName(String msgName) {
        this.msgName = msgName;
    }

    public String getSubMsgCode1() {
        return subMsgCode1;
    }

    public void setSubMsgCode1(String subMsgCode1) {
        this.subMsgCode1 = subMsgCode1;
    }

    public String getSubMsgCode2() {
        return subMsgCode2;
    }

    public void setSubMsgCode2(String subMsgCode2) {
        this.subMsgCode2 = subMsgCode2;
    }

    public String getSubMsgCode3() {
        return subMsgCode3;
    }

    public void setSubMsgCode3(String subMsgCode3) {
        this.subMsgCode3 = subMsgCode3;
    }

    public String getSubMsgCode4() {
        return subMsgCode4;
    }

    public void setSubMsgCode4(String subMsgCode4) {
        this.subMsgCode4 = subMsgCode4;
    }

    public boolean isCommonLotDispFlag() {
        return commonLotDispFlag;
    }

    public void setCommonLotDispFlag(boolean commonLotDispFlag) {this.commonLotDispFlag = commonLotDispFlag;}

    public int getRetWinNo() {
        return retWinNo;
    }

    public void setRetWinNo(int retWinNo) {
        this.retWinNo = retWinNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public SystemEntity getSystemEntity() {
        return systemEntity;
    }

    //	public void setSystemEntity(SystemEntity systemEntity) {
//		this.systemEntity = systemEntity;
//	}
    public String getDefaultLocationSys() {
        return defaultLocationSys;
    }

    public void setDefaultLocationSys(String defaultLocation) {this.defaultLocationSys = defaultLocation; }

    public AccessInfoEntity getAccessInfo() {
        return accessInfo;
    }

    public void setAccessInfo(AccessInfoEntity accessInfo) {
        this.accessInfo = accessInfo;
    }

    public List<QualityEntity> getQualities() {
        return qualities;
    }

    public QualityEntity getDefaultQuality() {
        return defaultQuality;
    }

    public void setDefaultQuality(QualityEntity defaultQuality) {this.defaultQuality = defaultQuality;}

    public List<ReasonEntity> getReasons() {return reasons;}

    public void setReasons(List<ReasonEntity> reasons) {this.reasons = reasons;}

    public List<Tmt280DataEntity> getTmt280Data() {
        return tmt280Data;
    }

    public void setTmt280Data(List<Tmt280DataEntity> tmt280Data) {
        this.tmt280Data = tmt280Data;
    }

    public List<BranchFromEntity> getBranch() {return branch;}

    public void setBranch(List<BranchFromEntity> branch) {this.branch = branch;}

    public ArrayList<Integer> getIndexImageSubMenu() {
        return indexImageSubMenu;
    }

    public void setIndexImageSubMenu(ArrayList<Integer> indexImageSubMenu) {this.indexImageSubMenu = indexImageSubMenu;}

    public String getDefaultBranch() {
        return defaultBranch;
    }

    public void setDefaultBranch(String defaultBranch) {
        this.defaultBranch = defaultBranch;
    }

    public List<ColorEntity> getColors() {return colors;}

    public void setColors(List<ColorEntity> colors) {this.colors = colors;}

    public List<SizeEntity> getSize() {return size;}

    public void setSize(List<SizeEntity> size) {this.size = size;}

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
    public void setDefaultCstm(String defaultCstm) {
        this.defaultCstm = defaultCstm;
    }
    public String getDefaultCstm() {
        return defaultCstm;
    }
}
