package vn.co.brycenvn.weather.localdb;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import vn.co.brycenvn.weather.localdb.dao.ItemDAO;

@Database(entities = {Item.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ItemDAO getItemDAO();
}