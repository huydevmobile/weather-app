package vn.co.brycenvn.weather.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import vn.co.brycenvn.weather.ForecastApplication;
import vn.co.brycenvn.weather.R;
import vn.co.brycenvn.weather.adapter.DatePickerTimeline;
import vn.co.brycenvn.weather.common.constant.ErrorCode;
import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.common.constant.MessageCode;
import vn.co.brycenvn.weather.common.managed.ManagedComponent;
import vn.co.brycenvn.weather.common.util.WmsUtil;
import vn.co.brycenvn.weather.config.ConfigurationManager;
import vn.co.brycenvn.weather.entity.DataBundle;
import vn.co.brycenvn.weather.entity.WeatherDataBundle;
import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.localdb.AppDatabase;
import vn.co.brycenvn.weather.localdb.Item;
import vn.co.brycenvn.weather.localdb.dao.ItemDAO;
import vn.co.brycenvn.weather.service.StateCallbackListener;
import vn.co.brycenvn.weather.service.consum.WeatherService;
import vn.co.brycenvn.weather.service.dto.DateWeatherDto;

import static vn.co.brycenvn.weather.common.constant.FHConst.MONTH_NAME;
import static vn.co.brycenvn.weather.common.constant.FHConst.WEEK_DAYS;
import static vn.co.brycenvn.weather.service.WmsService.setDismissDialog;
/**
 * @link hoquochuymufc@gmail.com - +84 0965976084
 * @author hoquochuy
 * Nals solution test exam for interview - nals_recruiter@nal.vn
 @link https://www.metaweather.com/api/location/1252431/yyy/mm/dd/ API link
 * Weather App
 */

public class WeatherActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TITLE = "FORECAST";
    private static final String TAG = "WeatherActivity";
    public static int screenWidth;
    private WeatherService weatherService = new WeatherService();
    private CommonEntity commonEntity = ForecastApplication.getRootCommonEntity();
    public WeatherDataBundle dataBundle = new WeatherDataBundle(commonEntity);
    private int cur_day, cur_month, cur_year;
    @BindView(R.id.iconWeather)
    ImageView iconWeather;
    @BindView(R.id.tempTV)
    TextView tempTV;
    @BindView(R.id.wtNameTV)
    TextView wtNameTV;
    @BindView(R.id.dateNameTV)
    TextView dateNameTV;
    @BindView(R.id.evrhumidity)
    TextView evrhumidity;
    @BindView(R.id.evr_predic)
    TextView evr_predic;
    @BindView(R.id.humidityTV)
    TextView humidityTV;
    @BindView(R.id.arc_progress)
    ArcProgress arcProgress;
    @BindView(R.id.arc_progress_predic)
    ArcProgress arc_progress_predic;
    @BindView(R.id.circleimg)
    ImageView circleimg;
    @BindView(R.id.dateTimeline)
    DatePickerTimeline dateTimeline;
    @BindView(R.id.noDataMsg)
    TextView noDataMsg;
    @BindView(R.id.mainui)
    LinearLayout mainui;
    @BindView(R.id.topui)
    LinearLayout topui;
    @BindView(R.id.centerui)
    LinearLayout centerui;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;
    private Snackbar snackbar;
    private int count;
    public ArrayList<ValueAnimator> valueAnimatorArrayList;
    private HashMap<String,Integer> listIcon=new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        getScreenWidth();
        setContentView(R.layout.activity_wheather);
        valueAnimatorArrayList = new ArrayList<>();
        ButterKnife.bind(this);
        setCaption();
        initOffIcon();
        dateTimeline.setWeatherActivity(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        displaySnackBar(MessageCode.ME000007,3000);
    }


    @Override
    protected CommonEntity getCommonEntity() {
        return null;
    }

    @Override
    protected List<ManagedComponent> setupControlList() {
        return null;
    }

    /**
     * @param year
     * @param month
     * @param day
     * @param online true if have internet so get data from @link https://www.metaweather.com/api/location/1252431/yyy/mm/dd/
     * false if offline mode, get data and stored in Room db
     * Call get data weather
     */
    public void callGetData(int year, int month, int day, boolean online) {
        setCurrentDate(year, month, day, online);
        Thread.currentThread().interrupt();
        for (ValueAnimator animator : valueAnimatorArrayList) {
            animator.removeAllListeners();
            animator.cancel();
        }
        noDataMsg.setVisibility(View.GONE);
        valueAnimatorArrayList.removeAll(valueAnimatorArrayList);
        if (checkNetwork(this)) {
            weatherService.getWheatherDataService(WeatherActivity.this, dataBundle, weatherServiceCallback, true, year, month, day, online);
        } else {
            getDataFromRoomDb(day, month, year);
        }
    }
    /**
     * @param year
     * @param month
     * @param day
     * @param online
     * save date to refresh view
     */
    private void setCurrentDate(int year, int month, int day, boolean online) {
        if (online){
            this.cur_year = year;
            this.cur_day = day;
            this.cur_month = month;
        }
    }
    /**
     * @param year
     * @param month
     * @param day
     * Get Data from local database
     */
    private void getDataFromRoomDb(int day, int month, int year) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    AppDatabase appDatabase = ((ForecastApplication) WeatherActivity.this.getApplication()).getDatabase();
                    ItemDAO itemDAO = appDatabase.getItemDAO();
                    List<Item> items = itemDAO.getItemByDate(day, month, year);
                    ArrayList<DateWeatherDto> infoE = dataBundle
                            .getDetailsEntity();
                    Iterator<DateWeatherDto> i = infoE.iterator();
                    while (i.hasNext()) {
                        i.next();
                        i.remove();
                    }
                    for (Item item : items) {
                        DateWeatherDto dateWeatherDto = new DateWeatherDto();
                        dateWeatherDto.setId(item.getId());
                        dateWeatherDto.setWeather_state_name(item.getWeather_state_name());
                        dateWeatherDto.setWeather_state_abbr(item.getWeather_state_abbr());
                        dateWeatherDto.setWind_direction_compass(item.getWind_direction_compass());
                        dateWeatherDto.setCreated(item.getCreated());
                        dateWeatherDto.setApplicable_date(item.getApplicable_date());
                        dateWeatherDto.setHumidity(item.getHumidity());
                        dateWeatherDto.setPredictability(item.getPredictability());
                        dateWeatherDto.setIcon(item.getIcon());
                        dateWeatherDto.setThe_temp(item.getThe_temp());
                        infoE.add(dateWeatherDto);
                    }
                    if (items.size()>0){
                        displayDataToUser(false);
                    }
                    else {
                        displayErrorMsg(ErrorCode.ERR_UNKNOW);
                        showUIData(false);
                    }
                } catch (ParseException e) {
                    displayErrorMsg(ErrorCode.ERR_UNKNOW);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * callback get data weather
     *@see #callGetData
     */
    private StateCallbackListener<DataBundle> weatherServiceCallback = new StateCallbackListener<DataBundle>() {
        @Override
        public void onSuccess(DataBundle data) {
            setDismissDialog();
            try {
                Integer iRet = data.getExtraVar(Integer.class, FHConst.RETURN_CODE);
                if (iRet != null) {
                    if (iRet != ErrorCode.SUCCESS) {
                        if (iRet == ErrorCode.SUCCESS_OFFLINE) {
                            count++;
                            if (count==21) {
                                displaySnackBar(MessageCode.ME000006,3000);
                            }
                        }
                        else{
                            displayErrorMsg(ErrorCode.ERR_UNKNOW);
                        }
                    }
                    else {
                        displayDataToUser(true);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                displayErrorMsg(ErrorCode.ERR_UNKNOW);
            }
        }


        @Override
        public void onFailure(DataBundle data) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showUIData(false);
                    mSwipeRefreshLayout.setRefreshing(false);
                    Integer iRet = data.getExtraVar(Integer.class, FHConst.RETURN_CODE);
                    displayErrorMsg(iRet);

                }
            });
        }
    };
    /**
     * @param msg String content message
     * @param duration time display
     * Display snackbar
     */
    private void displaySnackBar(String msg,int duration) {
        snackbar = Snackbar
                .make(mainLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.setDuration(duration);
        snackbar.show();
    }

    /**
     * @param iRet Error code
     */
    private void displayErrorMsg(Integer iRet) {
        String msg;
        switch (iRet) {
            case ErrorCode.FAIL_NETWORK:
                msg=MessageCode.ME000004;
                break;
            case ErrorCode.ERR_UNKNOW:
                msg=MessageCode.ERR20000;
                break;
            default:
                msg=MessageCode.ERRMSG1;
                break;

        }
        snackbar = Snackbar
                .make(mainLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
    /**
     * show or hide main UI when error
     * @param show
     *@see #callGetData
     */
    public void showUIData(boolean show) {
        if (show) {
            mainui.setVisibility(View.VISIBLE);
            centerui.setVisibility(View.VISIBLE);
            topui.setVisibility(View.VISIBLE);
            noDataMsg.setVisibility(View.GONE);
        } else {
            mainui.setVisibility(View.INVISIBLE);
            centerui.setVisibility(View.INVISIBLE);
            topui.setVisibility(View.INVISIBLE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    noDataMsg.setVisibility(View.VISIBLE);
                }
            });

        }
    }
    /**
     * @param list Find frequence commont string in a list
     * @return String
     */
    public static String findTheMostcCommon(List<String> list) {
        if (list == null || list.isEmpty())
            return null;
        Map<String, Integer> counterMap = new HashMap<String, Integer>();
        Integer maxValue = 0;
        String result = null;
        for (String valueAsKey : list) {
            Integer counter = counterMap.get(valueAsKey);
            counterMap.put(valueAsKey, counter == null ? 1 : counter + 1);
            counter = counterMap.get(valueAsKey);
            if (counter > maxValue) {
                maxValue = counter;
                result = valueAsKey;
            }
        }
        return result;
    }

    @Override
    public void onRefresh() {
        Toast.makeText(this, "Refresh", Toast.LENGTH_SHORT).show();
        callGetData(cur_year, cur_month, cur_day, true);
    }

    /**
     * @param online false,true display UI from data, online or from local db
     */
    private void displayDataToUser(boolean online) throws ParseException {
        final ArrayList<DateWeatherDto> infoE = dataBundle
                .getDetailsEntity();
        final ArrayList<String> list_weather_state_abbr = dataBundle
                .getList_weather_state_abbr();
        list_weather_state_abbr.removeAll(list_weather_state_abbr);
        final ArrayList<Float> list_temp = dataBundle
                .getList_temp();
        list_temp.removeAll(list_temp);
        final ArrayList<String> list_weather_state_name = dataBundle
                .getList_weather_state_name();
        list_weather_state_name.removeAll(list_weather_state_name);
        final ArrayList<Float> list_humidity = dataBundle
                .getList_humidity();
        list_humidity.removeAll(list_humidity);
        final ArrayList<Float> list_predictability = dataBundle
                .getList_predictability();
        list_predictability.removeAll(list_predictability);
        float everageTemp = 0;
        float everageHumidity = 0;
        float everagePredic = 0;
        String cur_time = null;
        for (DateWeatherDto i : infoE) {
            cur_time = i.getApplicable_date();
            list_weather_state_abbr.add(i.getWeather_state_abbr());
            list_weather_state_name.add(i.getWeather_state_name());
            list_temp.add(Float.valueOf(i.getThe_temp()));

            list_humidity.add(Float.valueOf(i.getHumidity()));
            list_predictability.add(Float.valueOf(i.getPredictability()));

            everageTemp += Float.valueOf(i.getThe_temp());
            everageHumidity += Float.valueOf(i.getHumidity());
            everagePredic += Float.valueOf(i.getPredictability());

            i.setIcon(ConfigurationManager
                    .getInstance().getConfig(FHConst.SERVICE_URL) + FHConst.BASE_URL_IMG + i.getWeather_state_abbr() + FHConst.IMG_TYPE);
        }
        if (list_humidity.size() > 0) {
            everageHumidity /= list_humidity.size();
        }
        if (list_predictability.size() > 0) {
            everagePredic /= list_predictability.size();
        }
        String iconName=findTheMostcCommon(list_weather_state_abbr);
        String finalUrlImage = ConfigurationManager
                .getInstance().getConfig(FHConst.SERVICE_URL) + FHConst.BASE_URL_IMG + iconName + FHConst.IMG_TYPE;
        float finalEverageTemp = everageTemp;
        String common_Weather_State_name = findTheMostcCommon(list_weather_state_name);
        String sDate1 = cur_time;
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(sDate1);
        Calendar specifiedDate = Calendar.getInstance();
        specifiedDate.setTime(date);
        float finalEverageHumidity = everageHumidity;
        float finalEveragePredic = everagePredic;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (list_temp.size() > 0) {
                    tempTV.setText(String.format("%.0f", finalEverageTemp / list_temp.size()) + (char) 0x00B0 + "c");
                }
                if (online){
                    Picasso.with(WeatherActivity.this).load(finalUrlImage)
                            .error(R.drawable.alert)
                            .into(iconWeather, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {
                                    setAnimation();
                                }
                                @Override
                                public void onError() {
                                    //do smth when there is picture loading error
                                }
                            });
                }
                else{
                    iconWeather.setBackground(null);
                    iconWeather.setBackground(AppCompatResources.getDrawable(getApplicationContext(),listIcon.get(iconName)));
                    setAnimation();
                }
                wtNameTV.setText(common_Weather_State_name);
                final int year = specifiedDate.get(Calendar.YEAR);
                final int month = specifiedDate.get(Calendar.MONTH);
                final int dayOfWeek = specifiedDate.get(Calendar.DAY_OF_WEEK);
                final String day = WmsUtil.capitalizeString(String.valueOf(specifiedDate.get(Calendar.DAY_OF_MONTH)));
                String wd = WmsUtil.capitalizeString(WEEK_DAYS[dayOfWeek]);
                String monthName = WmsUtil.capitalizeString(MONTH_NAME[month]);
                dateNameTV.setText(String.format("%s %s %s,%s", wd, monthName, day, year));
                ValueAnimator valueAnimator = ValueAnimator.ofInt(0, (int) finalEverageHumidity);
                ValueAnimator valueAnimatorPredic = ValueAnimator.ofInt(0, (int) finalEveragePredic);
                valueAnimator.setDuration(500);
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        evrhumidity.setText(valueAnimator.getAnimatedValue().toString() + "%");
                        arcProgress.setProgress((Integer) valueAnimator.getAnimatedValue());
                    }
                });
                valueAnimator.start();
                valueAnimatorPredic.setDuration(500);
                valueAnimatorPredic.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        evr_predic.setText(valueAnimatorPredic.getAnimatedValue().toString() + "%");
                        arc_progress_predic.setProgress((Integer) valueAnimatorPredic.getAnimatedValue());
                    }
                });
                valueAnimatorPredic.start();
                valueAnimatorArrayList.add(valueAnimator);
                valueAnimatorArrayList.add(valueAnimatorPredic);
                showUIData(true);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    /**
     * init icon for offline mode
     *@see #onCreate
     */
    private void initOffIcon() {
        listIcon.put("sn",R.drawable.sn);
        listIcon.put("sl",R.drawable.sl);
        listIcon.put("h",R.drawable.h);
        listIcon.put("t",R.drawable.t);
        listIcon.put("hr",R.drawable.hr);
        listIcon.put("lr",R.drawable.lr);
        listIcon.put("s",R.drawable.s);
        listIcon.put("hc",R.drawable.hc);
        listIcon.put("lc",R.drawable.lc);
        listIcon.put("c",R.drawable.c);
    }
    /**
     * set Animation for weather Icon
     * fade play with slide dowm, and then play rotation animation
     */
    private void setAnimation() {
        iconWeather.clearAnimation();
        ObjectAnimator move = ObjectAnimator.ofFloat(iconWeather, "translationY", -300f, 0f);
        move.setDuration(1000);
        ObjectAnimator alpha1 = ObjectAnimator.ofFloat(iconWeather, "alpha", 0f, 1);
        alpha1.setDuration(2000);

        AnimatorSet animset = new AnimatorSet();
        animset.play(alpha1).with(move);
        animset.start();
        animset.addListener(new AnimatorListenerAdapter() {

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                RotateAnimation rotate = new RotateAnimation(
                        0, 359,
                        Animation.RELATIVE_TO_SELF, 0.5f,
                        Animation.RELATIVE_TO_SELF, 0.5f
                );
                rotate.setDuration(1500);
                rotate.setRepeatCount(Animation.ABSOLUTE);
                iconWeather.startAnimation(rotate);
            }
        });
    }
    public int getScreenWidth() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        return size.x;
    }
}
