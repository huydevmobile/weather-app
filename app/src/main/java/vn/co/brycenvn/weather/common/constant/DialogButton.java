package vn.co.brycenvn.weather.common.constant;

public enum DialogButton {
    Yes,
    No,
    Cancel
}
