package vn.co.brycenvn.weather.common.constant;

public interface ScreenState {
    int STATE_1 = 0;
    int STATE_2 = 100;
    int STATE_3 = 200;
    int STATE_4 = 300;
    int STATE_5 = 400;
    int STATE_6 = 500;
    int STATE_7 = 600;
    int STATE_8 = 700;
    int STATE_9 = 800;
}
