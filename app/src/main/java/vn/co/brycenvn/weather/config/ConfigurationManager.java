package vn.co.brycenvn.weather.config;

import android.content.Context;

/**
 * Lớp quản lý thông tin cấu hình cho ứng dụng.
 * (Hiện tại chưa sử dụng)
 *
 * @author q_huy
 */
public class ConfigurationManager {
    private static final String CONFIG_FILE = "wms_config.cnf";
    private static ConfigurationManager instance;
    private UrlConfig config;


    private ConfigurationManager() {

    }

    public static void load(Context context) {
        getInstance().config = new UrlConfig(context);
    }

    public static ConfigurationManager getInstance() {
        if (instance == null) {
            instance = new ConfigurationManager();
        }
        return instance;
    }

    public String getConfig(String key) {
        return config.getConfig(key);
    }
    public String getConfig() {
        return config.getConfig();
    }
}
