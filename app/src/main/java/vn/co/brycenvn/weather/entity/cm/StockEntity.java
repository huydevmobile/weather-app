package vn.co.brycenvn.weather.entity.cm;

public class StockEntity {
    private String itemCd;
    private String itemNm;
    private String itemrNm;
    private String limitDate;
    private String prodDate;
    private String stckMngKey1;
    private String stckMngKey2;
    private String stckMngKey3;
    private String instrCd;
    private String limitDateInputKbn;
    private String productDateInputKbn;
    private String stckMngKey1InputKbn;
    private String stckMngKey2InputKbn;
    private String stckMngKey3InputKbn;
    private String serial;
    private String itemcode;
    private int sidtlno;
    private String siplnno;
    private String Kind;
    private String siRsltDate;
    private String divkbn;

    public String getDivkbn() { return divkbn; }

    public void setDivkbn(String divkbn) { this.divkbn = divkbn; }

    public String getItemCd() { return itemCd; }

    public String getSerial() { return serial; }

    public void setSerial(String serial) { this.serial = serial; }

    public String getItemcode() { return itemcode; }

    public void setItemcode(String itemcode) { this.itemcode = itemcode; }

    public int getSidtlno() { return sidtlno; }

    public void setSidtlno(int sidtlno) { this.sidtlno = sidtlno; }

    public String getSiplnno() { return siplnno; }

    public void setSiplnno(String siplnno) { this.siplnno = siplnno; }
    public void setItemCd(String itemCd) {
        this.itemCd = itemCd;
    }

    public String getItemNm() {
        return itemNm;
    }

    public void setItemNm(String itemNm) {
        this.itemNm = itemNm;
    }

    public String getItemrNm() {
        return itemrNm;
    }

    public void setItemrNm(String itemrNm) {
        this.itemrNm = itemrNm;
    }

    public String getLimitDate() {
        return limitDate;
    }

    public void setLimitDate(String limitDate) {
        this.limitDate = limitDate;
    }

    public String getStckMngKey1() {
        return stckMngKey1;
    }

    public void setStckMngKey1(String stckMngKey1) {
        this.stckMngKey1 = stckMngKey1;
    }

    public String getStckMngKey2() {
        return stckMngKey2;
    }

    public void setStckMngKey2(String stckMngKey2) {
        this.stckMngKey2 = stckMngKey2;
    }

    public String getStckMngKey3() {
        return stckMngKey3;
    }

    public void setStckMngKey3(String stckMngKey3) {
        this.stckMngKey3 = stckMngKey3;
    }

    public String getInstrCd() {
        return instrCd;
    }

    public void setInstrCd(String instrCd) {
        this.instrCd = instrCd;
    }

    public String getSiRsltDate() {
        return siRsltDate;
    }

    public void setSiRsltDate(String siRsltDate) {
        this.siRsltDate = siRsltDate;
    }
    public String getProdDate() {
        return prodDate;
    }

    public void setProdDate(String prodDate) {
        this.prodDate = prodDate;
    }
    public String getLimitDateInputKbn() {
        return limitDateInputKbn;
    }

    public void setLimitDateInputKbn(String limitDateInputKbn) {
        this.limitDateInputKbn = limitDateInputKbn;
    }

    public String getProductDateInputKbn() {
        return productDateInputKbn;
    }

    public void setProductDateInputKbn(String productDateInputKbn) {
        this.productDateInputKbn = productDateInputKbn;
    }

    public String getStckMngKey1InputKbn() {
        return stckMngKey1InputKbn;
    }

    public void setStckMngKey1InputKbn(String stckMngKey1InputKbn) {
        this.stckMngKey1InputKbn = stckMngKey1InputKbn;
    }

    public String getStckMngKey2InputKbn() {
        return stckMngKey2InputKbn;
    }

    public void setStckMngKey2InputKbn(String stckMngKey2InputKbn) {
        this.stckMngKey2InputKbn = stckMngKey2InputKbn;
    }

    public String getStckMngKey3InputKbn() {
        return stckMngKey3InputKbn;
    }

    public void setStckMngKey3InputKbn(String stckMngKey3InputKbn) {
        this.stckMngKey3InputKbn = stckMngKey3InputKbn;
    }

    public String getKind() {
        return Kind;
    }

    public void setKind(String kind) {
        Kind = kind;
    }
}
