package vn.co.brycenvn.weather.entity.cm;

import java.io.Serializable;

public class SystemEntity implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String cstmCd;
    private String sysFlg1;
    private String sysFlg2;
    private String sysFlg3;
    private String sysFlg4;
    private String sysFlg5;
    private String sysFlg6;
    private String sysFlg7;
    private String sysFlg8;
    private String sysFlg9;
    private String sysFlg10;
    private String sysFlg11;
    private String sysFlg12;
    private String sysFlg13;
    private String sysFlg14;
    private String sysFlg15;
    private String sysFlg16;
    private String sysFlg17;
    private String sysFlg18;
    private String sysFlg19;
    private String sysFlg20;
    private String sysFlg21;
    private String sysFlg22;
    private String sysFlg23;
    private String sysFlg24;
    private String sysFlg25;
    private String sysFlg26;
    private String sysFlg27;
    private String sysFlg28;
    private String sysFlg29;
    private String sysFlg30;
    private String sysFlg31;
    private String sysFlg32;
    private String sysFlg33;
    private String sysFlg34;
    private String sysFlg35;
    private String sysFlg36;
    private String sysFlg37;
    private String sysFlg38;
    private String sysFlg39;
    private String sysFlg40;
    private String sysFlg41;
    private String sysFlg42;
    private String sysFlg43;
    private String sysFlg44;
    private String sysFlg45;
    private String sysFlg46;
    private String sysFlg47;
    private String sysFlg48;
    private String sysFlg49;
    private String sysFlg50;
    private String sysKbn1;
    private String sysKbn2;
    private String sysKbn3;
    private String sysKbn4;
    private String sysKbn5;
    private String sysKbn6;
    private String sysKbn7;
    private String sysKbn8;
    private String sysKbn9;
    private String sysKbn10;
    private String sysKbn11;
    private String sysKbn12;
    private String sysKbn13;
    private String sysKbn14;
    private String sysKbn15;
    private String sysKbn16;
    private String sysKbn17;
    private String sysKbn18;
    private String sysKbn19;
    private String sysKbn20;
    private String sysKbn21;
    private String sysKbn22;
    private String sysKbn23;
    private String sysKbn24;
    private String sysKbn25;
    private String sysKbn26;
    private String sysKbn27;
    private String sysKbn28;
    private String sysKbn29;
    private String sysKbn30;
    private String sysKbn31;
    private String sysKbn32;
    private String sysKbn33;
    private String sysKbn34;
    private String sysKbn35;
    private String sysKbn36;
    private String sysKbn37;
    private String sysKbn38;
    private String sysKbn39;
    private String sysKbn40;
    private String sysKbn41;
    private String sysKbn42;
    private String sysKbn43;
    private String sysKbn44;
    private String sysKbn45;
    private String sysKbn46;
    private String sysKbn47;
    private String sysKbn48;
    private String sysKbn49;
    private String sysKbn50;
    private String sysNumVal1;
    private String sysNumVal2;
    private String sysNumVal3;
    private String sysNumVal4;
    private String sysNumVal5;
    private String sysNumVal6;
    private String sysNumVal7;
    private String sysNumVal8;
    private String sysNumVal9;
    private String sysNumVal10;
    private String sysNumVal11;
    private String sysNumVal12;
    private String sysNumVal13;
    private String sysNumVal14;
    private String sysNumVal15;
    private String sysNumVal16;
    private String sysNumVal17;
    private String sysNumVal18;
    private String sysNumVal19;
    private String sysNumVal20;

    private String sidivKbn;
    private String hndivKbn;
    private String sodivKbn;
    private String mkdivKbn;
    private String addivKbn;
    private String hnlctnCd;
    private String tmLctnCd;
    private String dfltQltyCd;
    private boolean checkLengthCut = false;
    private boolean isScan2D = false;


    public String getCstmCd() {
        return cstmCd;
    }

    public void setCstmCd(String cstmCd) {
        this.cstmCd = cstmCd;
    }

    public String getSysFlg1() {
        return sysFlg1;
    }

    public void setSysFlg1(String sysFlg1) {
        this.sysFlg1 = sysFlg1;
    }

    public String getSysFlg2() {
        return sysFlg2;
    }

    public void setSysFlg2(String sysFlg2) {
        this.sysFlg2 = sysFlg2;
    }

    public String getSysFlg3() {
        return sysFlg3;
    }

    public void setSysFlg3(String sysFlg3) {
        this.sysFlg3 = sysFlg3;
    }

    public String getSysFlg4() {
        return sysFlg4;
    }

    public void setSysFlg4(String sysFlg4) {
        this.sysFlg4 = sysFlg4;
    }

    public String getSysFlg5() {
        return sysFlg5;
    }

    public void setSysFlg5(String sysFlg5) {
        this.sysFlg5 = sysFlg5;
    }

    public String getSysFlg6() {
        return sysFlg6;
    }

    public void setSysFlg6(String sysFlg6) {
        this.sysFlg6 = sysFlg6;
    }

    public String getSysFlg7() {
        return sysFlg7;
    }

    public void setSysFlg7(String sysFlg7) {
        this.sysFlg7 = sysFlg7;
    }

    public String getSysFlg8() {
        return sysFlg8;
    }

    public void setSysFlg8(String sysFlg8) {
        this.sysFlg8 = sysFlg8;
    }

    public String getSysFlg9() {
        return sysFlg9;
    }

    public void setSysFlg9(String sysFlg9) {
        this.sysFlg9 = sysFlg9;
    }

    public String getSysFlg10() {
        return sysFlg10;
    }

    public void setSysFlg10(String sysFlg10) {
        this.sysFlg10 = sysFlg10;
    }

    public String getSysFlg11() {
        return sysFlg11;
    }

    public void setSysFlg11(String sysFlg11) {
        this.sysFlg11 = sysFlg11;
    }

    public String getSysFlg12() {
        return sysFlg12;
    }

    public void setSysFlg12(String sysFlg12) {
        this.sysFlg12 = sysFlg12;
    }

    public String getSysFlg13() {
        return sysFlg13;
    }

    public void setSysFlg13(String sysFlg13) {
        this.sysFlg13 = sysFlg13;
    }

    public String getSysFlg14() {
        return sysFlg14;
    }

    public void setSysFlg14(String sysFlg14) {
        this.sysFlg14 = sysFlg14;
    }

    public String getSysFlg15() {
        return sysFlg15;
    }

    public void setSysFlg15(String sysFlg15) {
        this.sysFlg15 = sysFlg15;
    }

    public String getSysFlg16() {
        return sysFlg16;
    }

    public void setSysFlg16(String sysFlg16) {
        this.sysFlg16 = sysFlg16;
    }

    public String getSysFlg17() {
        return sysFlg17;
    }

    public void setSysFlg17(String sysFlg17) {
        this.sysFlg17 = sysFlg17;
    }

    public String getSysFlg18() {
        return sysFlg18;
    }

    public void setSysFlg18(String sysFlg18) {
        this.sysFlg18 = sysFlg18;
    }

    public String getSysFlg19() {
        return sysFlg19;
    }

    public void setSysFlg19(String sysFlg19) {
        this.sysFlg19 = sysFlg19;
    }

    public String getSysFlg20() {
        return sysFlg20;
    }

    public void setSysFlg20(String sysFlg20) {
        this.sysFlg20 = sysFlg20;
    }

    public String getSysFlg21() {
        return sysFlg21;
    }

    public void setSysFlg21(String sysFlg21) {
        this.sysFlg21 = sysFlg21;
    }

    public String getSysFlg22() {
        return sysFlg22;
    }

    public void setSysFlg22(String sysFlg22) {
        this.sysFlg22 = sysFlg22;
    }

    public String getSysFlg23() {
        return sysFlg23;
    }

    public void setSysFlg23(String sysFlg23) {
        this.sysFlg23 = sysFlg23;
    }

    public String getSysFlg24() {
        return sysFlg24;
    }

    public void setSysFlg24(String sysFlg24) {
        this.sysFlg24 = sysFlg24;
    }

    public String getSysFlg25() {
        return sysFlg25;
    }

    public void setSysFlg25(String sysFlg25) {
        this.sysFlg25 = sysFlg25;
    }

    public String getSysFlg26() {
        return sysFlg26;
    }

    public void setSysFlg26(String sysFlg26) {
        this.sysFlg26 = sysFlg26;
    }

    public String getSysFlg27() {
        return sysFlg27;
    }

    public void setSysFlg27(String sysFlg27) {
        this.sysFlg27 = sysFlg27;
    }

    public String getSysFlg28() {
        return sysFlg28;
    }

    public void setSysFlg28(String sysFlg28) {
        this.sysFlg28 = sysFlg28;
    }

    public String getSysFlg29() {
        return sysFlg29;
    }

    public void setSysFlg29(String sysFlg29) {
        this.sysFlg29 = sysFlg29;
    }

    public String getSysFlg30() {
        return sysFlg30;
    }

    public void setSysFlg30(String sysFlg30) {
        this.sysFlg30 = sysFlg30;
    }

    public String getSysFlg31() {
        return sysFlg31;
    }

    public void setSysFlg31(String sysFlg31) {
        this.sysFlg31 = sysFlg31;
    }

    public String getSysFlg32() {
        return sysFlg32;
    }

    public void setSysFlg32(String sysFlg32) {
        this.sysFlg32 = sysFlg32;
    }

    public String getSysFlg33() {
        return sysFlg33;
    }

    public void setSysFlg33(String sysFlg33) {
        this.sysFlg33 = sysFlg33;
    }

    public String getSysFlg34() {
        return sysFlg34;
    }

    public void setSysFlg34(String sysFlg34) {
        this.sysFlg34 = sysFlg34;
    }

    public String getSysFlg35() {
        return sysFlg35;
    }

    public void setSysFlg35(String sysFlg35) {
        this.sysFlg35 = sysFlg35;
    }

    public String getSysFlg36() {
        return sysFlg36;
    }

    public void setSysFlg36(String sysFlg36) {
        this.sysFlg36 = sysFlg36;
    }

    public String getSysFlg37() {
        return sysFlg37;
    }

    public void setSysFlg37(String sysFlg37) {
        this.sysFlg37 = sysFlg37;
    }

    public String getSysFlg38() {
        return sysFlg38;
    }

    public void setSysFlg38(String sysFlg38) {
        this.sysFlg38 = sysFlg38;
    }

    public String getSysFlg39() {
        return sysFlg39;
    }

    public void setSysFlg39(String sysFlg39) {
        this.sysFlg39 = sysFlg39;
    }

    public String getSysFlg40() {
        return sysFlg40;
    }

    public void setSysFlg40(String sysFlg40) {
        this.sysFlg40 = sysFlg40;
    }

    public String getSysFlg41() {
        return sysFlg41;
    }

    public void setSysFlg41(String sysFlg41) {
        this.sysFlg41 = sysFlg41;
    }

    public String getSysFlg42() {
        return sysFlg42;
    }

    public void setSysFlg42(String sysFlg42) {
        this.sysFlg42 = sysFlg42;
    }

    public String getSysFlg43() {
        return sysFlg43;
    }

    public void setSysFlg43(String sysFlg43) {
        this.sysFlg43 = sysFlg43;
    }

    public String getSysFlg44() {
        return sysFlg44;
    }

    public void setSysFlg44(String sysFlg44) {
        this.sysFlg44 = sysFlg44;
    }

    public String getSysFlg45() {
        return sysFlg45;
    }

    public void setSysFlg45(String sysFlg45) {
        this.sysFlg45 = sysFlg45;
    }

    public String getSysFlg46() {
        return sysFlg46;
    }

    public void setSysFlg46(String sysFlg46) {
        this.sysFlg46 = sysFlg46;
    }

    public String getSysFlg47() {
        return sysFlg47;
    }

    public void setSysFlg47(String sysFlg47) {
        this.sysFlg47 = sysFlg47;
    }

    public String getSysFlg48() {
        return sysFlg48;
    }

    public void setSysFlg48(String sysFlg48) {
        this.sysFlg48 = sysFlg48;
    }

    public String getSysFlg49() {
        return sysFlg49;
    }

    public void setSysFlg49(String sysFlg49) {
        this.sysFlg49 = sysFlg49;
    }

    public String getSysFlg50() {
        return sysFlg50;
    }

    public void setSysFlg50(String sysFlg50) {
        this.sysFlg50 = sysFlg50;
    }

    public String getSysKbn1() {
        return sysKbn1;
    }

    public void setSysKbn1(String sysKbn1) {
        this.sysKbn1 = sysKbn1;
    }

    public String getSysKbn2() {
        return sysKbn2;
    }

    public void setSysKbn2(String sysKbn2) {
        this.sysKbn2 = sysKbn2;
    }

    public String getSysKbn3() {
        return sysKbn3;
    }

    public void setSysKbn3(String sysKbn3) {
        this.sysKbn3 = sysKbn3;
    }

    public String getSysKbn4() {
        return sysKbn4;
    }

    public void setSysKbn4(String sysKbn4) {
        this.sysKbn4 = sysKbn4;
    }

    public String getSysKbn5() {
        return sysKbn5;
    }

    public void setSysKbn5(String sysKbn5) {
        this.sysKbn5 = sysKbn5;
    }

    public String getSysKbn6() {
        return sysKbn6;
    }

    public void setSysKbn6(String sysKbn6) {
        this.sysKbn6 = sysKbn6;
    }

    public String getSysKbn7() {
        return sysKbn7;
    }

    public void setSysKbn7(String sysKbn7) {
        this.sysKbn7 = sysKbn7;
    }

    public String getSysKbn8() {
        return sysKbn8;
    }

    public void setSysKbn8(String sysKbn8) {
        this.sysKbn8 = sysKbn8;
    }

    public String getSysKbn9() {
        return sysKbn9;
    }

    public void setSysKbn9(String sysKbn9) {
        this.sysKbn9 = sysKbn9;
    }

    public String getSysKbn10() {
        return sysKbn10;
    }

    public void setSysKbn10(String sysKbn10) {
        this.sysKbn10 = sysKbn10;
    }

    public String getSysKbn11() {
        return sysKbn11;
    }

    public void setSysKbn11(String sysKbn11) {
        this.sysKbn11 = sysKbn11;
    }

    public String getSysKbn12() {
        return sysKbn12;
    }

    public void setSysKbn12(String sysKbn12) {
        this.sysKbn12 = sysKbn12;
    }

    public String getSysKbn13() {
        return sysKbn13;
    }

    public void setSysKbn13(String sysKbn13) {
        this.sysKbn13 = sysKbn13;
    }

    public String getSysKbn14() {
        return sysKbn14;
    }

    public void setSysKbn14(String sysKbn14) {
        this.sysKbn14 = sysKbn14;
    }

    public String getSysKbn15() {
        return sysKbn15;
    }

    public void setSysKbn15(String sysKbn15) {
        this.sysKbn15 = sysKbn15;
    }

    public String getSysKbn16() {
        return sysKbn16;
    }

    public void setSysKbn16(String sysKbn16) {
        this.sysKbn16 = sysKbn16;
    }

    public String getSysKbn17() {
        return sysKbn17;
    }

    public void setSysKbn17(String sysKbn17) {
        this.sysKbn17 = sysKbn17;
    }

    public String getSysKbn18() {
        return sysKbn18;
    }

    public void setSysKbn18(String sysKbn18) {
        this.sysKbn18 = sysKbn18;
    }

    public String getSysKbn19() {
        return sysKbn19;
    }

    public void setSysKbn19(String sysKbn19) {
        this.sysKbn19 = sysKbn19;
    }

    public String getSysKbn20() {
        return sysKbn20;
    }

    public void setSysKbn20(String sysKbn20) {
        this.sysKbn20 = sysKbn20;
    }

    public String getSysKbn21() {
        return sysKbn21;
    }

    public void setSysKbn21(String sysKbn21) {
        this.sysKbn21 = sysKbn21;
    }

    public String getSysKbn22() {
        return sysKbn22;
    }

    public void setSysKbn22(String sysKbn22) {
        this.sysKbn22 = sysKbn22;
    }

    public String getSysKbn23() {
        return sysKbn23;
    }

    public void setSysKbn23(String sysKbn23) {
        this.sysKbn23 = sysKbn23;
    }

    public String getSysKbn24() {
        return sysKbn24;
    }

    public void setSysKbn24(String sysKbn24) {
        this.sysKbn24 = sysKbn24;
    }

    public String getSysKbn25() {
        return sysKbn25;
    }

    public void setSysKbn25(String sysKbn25) {
        this.sysKbn25 = sysKbn25;
    }

    public String getSysKbn26() {
        return sysKbn26;
    }

    public void setSysKbn26(String sysKbn26) {
        this.sysKbn26 = sysKbn26;
    }

    public String getSysKbn27() {
        return sysKbn27;
    }

    public void setSysKbn27(String sysKbn27) {
        this.sysKbn27 = sysKbn27;
    }

    public String getSysKbn28() {
        return sysKbn28;
    }

    public void setSysKbn28(String sysKbn28) {
        this.sysKbn28 = sysKbn28;
    }

    public String getSysKbn29() {
        return sysKbn29;
    }

    public void setSysKbn29(String sysKbn29) {
        this.sysKbn29 = sysKbn29;
    }

    public String getSysKbn30() {
        return sysKbn30;
    }

    public void setSysKbn30(String sysKbn30) {
        this.sysKbn30 = sysKbn30;
    }

    public String getSysKbn31() {
        return sysKbn31;
    }

    public void setSysKbn31(String sysKbn31) {
        this.sysKbn31 = sysKbn31;
    }

    public String getSysKbn32() {
        return sysKbn32;
    }

    public void setSysKbn32(String sysKbn32) {
        this.sysKbn32 = sysKbn32;
    }

    public String getSysKbn33() {
        return sysKbn33;
    }

    public void setSysKbn33(String sysKbn33) {
        this.sysKbn33 = sysKbn33;
    }

    public String getSysKbn34() {
        return sysKbn34;
    }

    public void setSysKbn34(String sysKbn34) {
        this.sysKbn34 = sysKbn34;
    }

    public String getSysKbn35() {
        return sysKbn35;
    }

    public void setSysKbn35(String sysKbn35) {
        this.sysKbn35 = sysKbn35;
    }

    public String getSysKbn36() {
        return sysKbn36;
    }

    public void setSysKbn36(String sysKbn36) {
        this.sysKbn36 = sysKbn36;
    }

    public String getSysKbn37() {
        return sysKbn37;
    }

    public void setSysKbn37(String sysKbn37) {
        this.sysKbn37 = sysKbn37;
    }

    public String getSysKbn38() {
        return sysKbn38;
    }

    public void setSysKbn38(String sysKbn38) {
        this.sysKbn38 = sysKbn38;
    }

    public String getSysKbn39() {
        return sysKbn39;
    }

    public void setSysKbn39(String sysKbn39) {
        this.sysKbn39 = sysKbn39;
    }

    public String getSysKbn40() {
        return sysKbn40;
    }

    public void setSysKbn40(String sysKbn40) {
        this.sysKbn40 = sysKbn40;
    }

    public String getSysKbn41() {
        return sysKbn41;
    }

    public void setSysKbn41(String sysKbn41) {
        this.sysKbn41 = sysKbn41;
    }

    public String getSysKbn42() {
        return sysKbn42;
    }

    public void setSysKbn42(String sysKbn42) {
        this.sysKbn42 = sysKbn42;
    }

    public String getSysKbn43() {
        return sysKbn43;
    }

    public void setSysKbn43(String sysKbn43) {
        this.sysKbn43 = sysKbn43;
    }

    public String getSysKbn44() {
        return sysKbn44;
    }

    public void setSysKbn44(String sysKbn44) {
        this.sysKbn44 = sysKbn44;
    }

    public String getSysKbn45() {
        return sysKbn45;
    }

    public void setSysKbn45(String sysKbn45) {
        this.sysKbn45 = sysKbn45;
    }

    public String getSysKbn46() {
        return sysKbn46;
    }

    public void setSysKbn46(String sysKbn46) {
        this.sysKbn46 = sysKbn46;
    }

    public String getSysKbn47() {
        return sysKbn47;
    }

    public void setSysKbn47(String sysKbn47) {
        this.sysKbn47 = sysKbn47;
    }

    public String getSysKbn48() {
        return sysKbn48;
    }

    public void setSysKbn48(String sysKbn48) {
        this.sysKbn48 = sysKbn48;
    }

    public String getSysKbn49() {
        return sysKbn49;
    }

    public void setSysKbn49(String sysKbn49) {
        this.sysKbn49 = sysKbn49;
    }

    public String getSysKbn50() {
        return sysKbn50;
    }

    public void setSysKbn50(String sysKbn50) {
        this.sysKbn50 = sysKbn50;
    }

    public String getSysNumVal1() {
        return sysNumVal1;
    }

    public void setSysNumVal1(String sysNumVal1) {
        this.sysNumVal1 = sysNumVal1;
    }

    public String getSysNumVal2() {
        return sysNumVal2;
    }

    public void setSysNumVal2(String sysNumVal2) {
        this.sysNumVal2 = sysNumVal2;
    }

    public String getSysNumVal3() {
        return sysNumVal3;
    }

    public void setSysNumVal3(String sysNumVal3) {
        this.sysNumVal3 = sysNumVal3;
    }

    public String getSysNumVal4() {
        return sysNumVal4;
    }

    public void setSysNumVal4(String sysNumVal4) {
        this.sysNumVal4 = sysNumVal4;
    }

    public String getSysNumVal5() {
        return sysNumVal5;
    }

    public void setSysNumVal5(String sysNumVal5) {
        this.sysNumVal5 = sysNumVal5;
    }

    public String getSysNumVal6() {
        return sysNumVal6;
    }

    public void setSysNumVal6(String sysNumVal6) {
        this.sysNumVal6 = sysNumVal6;
    }

    public String getSysNumVal7() {
        return sysNumVal7;
    }

    public void setSysNumVal7(String sysNumVal7) {
        this.sysNumVal7 = sysNumVal7;
    }

    public String getSysNumVal8() {
        return sysNumVal8;
    }

    public void setSysNumVal8(String sysNumVal8) {
        this.sysNumVal8 = sysNumVal8;
    }

    public String getSysNumVal9() {
        return sysNumVal9;
    }

    public void setSysNumVal9(String sysNumVal9) {
        this.sysNumVal9 = sysNumVal9;
    }

    public String getSysNumVal10() {
        return sysNumVal10;
    }

    public void setSysNumVal10(String sysNumVal10) {
        this.sysNumVal10 = sysNumVal10;
    }

    public String getSysNumVal11() {
        return sysNumVal11;
    }

    public void setSysNumVal11(String sysNumVal11) {
        this.sysNumVal11 = sysNumVal11;
    }

    public String getSysNumVal12() {
        return sysNumVal12;
    }

    public void setSysNumVal12(String sysNumVal12) {
        this.sysNumVal12 = sysNumVal12;
    }

    public String getSysNumVal13() {
        return sysNumVal13;
    }

    public void setSysNumVal13(String sysNumVal13) {
        this.sysNumVal13 = sysNumVal13;
    }

    public String getSysNumVal14() {
        return sysNumVal14;
    }

    public void setSysNumVal14(String sysNumVal14) {
        this.sysNumVal14 = sysNumVal14;
    }

    public String getSysNumVal15() {
        return sysNumVal15;
    }

    public void setSysNumVal15(String sysNumVal15) {
        this.sysNumVal15 = sysNumVal15;
    }

    public String getSysNumVal16() {
        return sysNumVal16;
    }

    public void setSysNumVal16(String sysNumVal16) {
        this.sysNumVal16 = sysNumVal16;
    }

    public String getSysNumVal17() {
        return sysNumVal17;
    }

    public void setSysNumVal17(String sysNumVal17) {
        this.sysNumVal17 = sysNumVal17;
    }

    public String getSysNumVal18() {
        return sysNumVal18;
    }

    public void setSysNumVal18(String sysNumVal18) {
        this.sysNumVal18 = sysNumVal18;
    }

    public String getSysNumVal19() {
        return sysNumVal19;
    }

    public void setSysNumVal19(String sysNumVal19) {
        this.sysNumVal19 = sysNumVal19;
    }

    public String getSysNumVal20() {
        return sysNumVal20;
    }

    public void setSysNumVal20(String sysNumVal20) {
        this.sysNumVal20 = sysNumVal20;
    }

    public String getSidivKbn() {
        return sidivKbn;
    }

    public void setSidivKbn(String sidivKbn) {
        this.sidivKbn = sidivKbn;
    }

    public String getHndivKbn() {
        return hndivKbn;
    }

    public void setHndivKbn(String hndivKbn) {
        this.hndivKbn = hndivKbn;
    }

    public String getSodivKbn() {
        return sodivKbn;
    }

    public void setSodivKbn(String sodivKbn) {
        this.sodivKbn = sodivKbn;
    }

    public String getMkdivKbn() {
        return mkdivKbn;
    }

    public void setMkdivKbn(String mkdivKbn) {
        this.mkdivKbn = mkdivKbn;
    }

    public String getAddivKbn() {
        return addivKbn;
    }

    public void setAddivKbn(String addivKbn) {
        this.addivKbn = addivKbn;
    }

    public String getHnlctnCd() {
        return hnlctnCd;
    }

    public void setHnlctnCd(String hnlctnCd) {
        this.hnlctnCd = hnlctnCd;
    }

    public String getTmLctnCd() {
        return tmLctnCd;
    }

    public void setTmLctnCd(String tmLctnCd) {
        this.tmLctnCd = tmLctnCd;
    }

    public String getDfltQltyCd() {
        return dfltQltyCd;
    }

    public void setDfltQltyCd(String dfltQltyCd) {
        this.dfltQltyCd = dfltQltyCd;
    }


    public boolean isCheckLengthCut() {
        return checkLengthCut;
    }

    public void setCheckLengthCut(boolean checkLengthCut) { this.checkLengthCut = checkLengthCut; }

    public boolean isScan2D() { return isScan2D; }

    public void setScan2D(boolean scan2D) { isScan2D = scan2D; }
}
