package vn.co.brycenvn.weather.config;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import vn.co.brycenvn.weather.common.constant.FHConst;

public class UrlConfig {
    private Map<String, String> mapConfig = new HashMap<String, String>();
    private String local ="http://192.168.3.60:8080/";
    private String baseApi="https://www.metaweather.com/";

    private String urlConfig;
    public UrlConfig(Context context) {
        // TODO: temporaty set value
        SharedPreferences sharedPreferences = context.getSharedPreferences("local", Context.MODE_PRIVATE);
        String url = sharedPreferences.getString("SERVICE_URL", null);
        if (url == null || url.equals("")) {
            url = baseApi;
            urlConfig = url;
        }
        setConfig(FHConst.SERVICE_URL, url);
        setConfig(FHConst.HOME_URL, url + "/index.html");
    }

    final void setConfig(String key, String value) {
        mapConfig.put(key, value);
    }

    public String getConfig(String key) {
        if (mapConfig.containsKey(key)) {
            return mapConfig.get(key);
        }
        return "";
    }
    final String getConfig() {
        return urlConfig;
    }
}
