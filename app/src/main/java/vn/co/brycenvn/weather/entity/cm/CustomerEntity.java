package vn.co.brycenvn.weather.entity.cm;

public class CustomerEntity {
    private String cstmCode;
    private String cstmName;
    private String cstmRName;


    public CustomerEntity() {
        super();
    }

    public CustomerEntity(String cstmCode, String cstmName, String cstmRName) {
        super();
        this.cstmCode = cstmCode;
        this.cstmName = cstmName;
        this.cstmRName = cstmRName;
    }

    public String getCstmCode() {
        return cstmCode;
    }

    public void setCstmCode(String cstmCode) {
        this.cstmCode = cstmCode;
    }

    public String getCstmName() {
        return cstmName;
    }

    public void setCstmName(String cstmName) {
        this.cstmName = cstmName;
    }

    public String getCstmRName() {
        return cstmRName;
    }

    public void setCstmRName(String cstmRName) {
        this.cstmRName = cstmRName;
    }

}
