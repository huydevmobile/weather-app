package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

/**
 * Created by q_huy on 2/22/2018.
 */

public class ColorEntity implements WmsEntity {
    private String colorCode;
    private String colorName;

    public ColorEntity(String colorCode, String colorName) {
        this.colorCode = colorCode;
        this.colorName = colorName;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }
}
