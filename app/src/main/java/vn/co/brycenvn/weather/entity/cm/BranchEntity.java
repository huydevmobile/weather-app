package vn.co.brycenvn.weather.entity.cm;

public class BranchEntity {
    private String brnchCode;
    private String brnchName;
    private String brnchRName;


    public BranchEntity() {
        super();
    }

    public BranchEntity(String brnchCode, String brnchName, String brnchRName) {
        super();
        this.brnchCode = brnchCode;
        this.brnchName = brnchName;
        this.brnchRName = brnchRName;
    }

    public String getBrnchCode() {
        return brnchCode;
    }

    public void setBrnchCode(String brnchCode) {
        this.brnchCode = brnchCode;
    }

    public String getBrnchName() {
        return brnchName;
    }

    public void setBrnchName(String brnchName) {
        this.brnchName = brnchName;
    }

    public String getBrnchRName() {
        return brnchRName;
    }

    public void setBrnchRName(String brnchRName) {
        this.brnchRName = brnchRName;
    }


}
