package vn.co.brycenvn.weather.service;

import java.util.HashMap;
import java.util.Map;

public class Request {
    public String url;
    Map<String, Object> mapParams;

    public Request(String url) {
        mapParams = new HashMap<String, Object>();
        this.url = url;
    }

    public void setParam(String key, Object value) {
        mapParams.put(key, value);
    }

    public String getRequestUrl() {
        return url;
    }

    public Map<String, Object> getParams() {
        return mapParams;
    }
}
