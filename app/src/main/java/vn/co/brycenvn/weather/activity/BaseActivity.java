package vn.co.brycenvn.weather.activity;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.util.List;

import vn.co.brycenvn.weather.ForecastApplication;
import vn.co.brycenvn.weather.R;
import vn.co.brycenvn.weather.common.constant.ErrorCode;
import vn.co.brycenvn.weather.common.constant.MessageCode;
import vn.co.brycenvn.weather.common.constant.MessageType;
import vn.co.brycenvn.weather.common.managed.ManagedComponent;
import vn.co.brycenvn.weather.common.resourcemng.FreshResource;
import vn.co.brycenvn.weather.common.resourcemng.ResourceManager;
import vn.co.brycenvn.weather.common.resourcemng.dto.MessageDto;
import vn.co.brycenvn.weather.common.util.WmsUtil;
import vn.co.brycenvn.weather.entity.DataBundle;

import vn.co.brycenvn.weather.entity.cm.CommonEntity;

public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    public String screenId;
    protected CommonEntity commonEntity;
    public BroadcastReceiver mBroadcastReceiver;
    protected int iMinHeight = 50;
    public static int langKind = 0;
    protected Typeface externalFont;
    protected Typeface externalBFont;
    protected Typeface RegularFont;
    Vibrator vibrate;
    protected ForecastApplication app;
    public Boolean onRequest = false;
    ActionBar bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        this.commonEntity = (CommonEntity) getIntent().getSerializableExtra("data");
        externalFont = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        externalBFont = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        RegularFont = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Bold.ttf");
        bar = getActionBar();
        if (bar != null) {
            bar.setIcon(R.mipmap.ic_launcher);
        }
        if (bar != null) {
            bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#FFE7E7E7")));

        }
        vibrate = (Vibrator) BaseActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void setCaption() {
        try {
            List<ManagedComponent> lstComponent = setupControlList();
            for (ManagedComponent comp : lstComponent) {
                if (comp.getUiComponent() instanceof TextView) {
                    ((TextView) comp.getUiComponent()).setGravity(Gravity.CENTER_VERTICAL);
                    ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                    ((TextView) comp.getUiComponent()).setTypeface(externalFont);

                    if (comp.getGroup().equals("TvSubTitle")) {
                        comp.getUiComponent()
                                .setBackgroundColor(getResources().getColor(R.color.sub_title_background));
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.title_color));
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface(((TextView) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                        comp.getUiComponent().setPadding(10, 0, 0, 0);
                    }
                    if (comp.getGroup().equals("TvDetail")) {
                        comp.getUiComponent().setPadding(10, 0, 0, 0);
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.edit_color));
                    }
                    if (comp.getGroup().equals("TvDetail2")) {
                        comp.getUiComponent().setPadding(10, 0, 0, 0);
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface((((TextView) comp.getUiComponent()).getTypeface()), Typeface.BOLD);
                        ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.error_color));

                    }
                    if (comp.getGroup().equals("TvDetail3")) {
                        comp.getUiComponent().setPadding(0, 0, 0, 0);
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface((((TextView) comp.getUiComponent()).getTypeface()), Typeface.BOLD);
                        ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.error_color));

                    }
                    if (comp.getGroup().equals("TvTitle")) {
                        comp.getUiComponent()
                                .setBackgroundColor(getResources().getColor(R.color.title_background));
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.title_color));
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface(((TextView) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                        comp.getUiComponent().setPadding(5, 0, 0, 0);
                    }
                    if (comp.getGroup().equals("TvTitle2")) {
                        comp.getUiComponent()
                                .setBackgroundColor(getResources().getColor(R.color.title_background));
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.error_color));
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface(((TextView) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                        comp.getUiComponent().setPadding(5, 0, 0, 0);
                    }
                    if (comp.getGroup().equals("TvTitle3")) {
                        comp.getUiComponent()
                                .setBackgroundColor(getResources().getColor(R.color.title_background));
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.title_color));
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setGravity(Gravity.CENTER);
                        ((TextView) comp.getUiComponent()).setTypeface(((TextView) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                    }
                    if (comp.getGroup().equals("TvTitle4")) {
                        comp.getUiComponent()
                                .setBackgroundColor(getResources().getColor(R.color.title_background));
                        ((TextView) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.edit_color));
                        ((TextView) comp.getUiComponent()).setMinHeight(iMinHeight);
                        ((TextView) comp.getUiComponent()).setTypeface(((TextView) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                        comp.getUiComponent().setPadding(5, 0, 0, 0);
                    }
                    if (!this.screenId.equals("SHCM0010")) {
                        ((TextView) comp.getUiComponent()).setText(
                                ResourceManager.getText(this.screenId, Integer.parseInt(comp.getData().toString())));
                        ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                        if (comp.getGroup().equals("TvTitle3")) {
                            ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
                        }
                    } else {
                        ((TextView) comp.getUiComponent()).setTypeface(externalBFont);
                        ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);

                        if (comp.getGroup().equals("TvTitle")) {
                            ((TextView) comp.getUiComponent())
                                    .setTextColor(getResources().getColor(R.color.edit_color));
                        }
                        if (comp.getGroup().equals("TvControl")) {
                            ((TextView) comp.getUiComponent()).setTypeface(externalFont);
                            ((TextView) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                            ((TextView) comp.getUiComponent()).setGravity(Gravity.CENTER);
                        }

                    }
                }
                if (comp.getUiComponent() instanceof EditText || comp.getUiComponent() instanceof AutoCompleteTextView) {
                    ((EditText) comp.getUiComponent()).setMinHeight(iMinHeight);
                    ((EditText) comp.getUiComponent()).setTextSize(TypedValue.COMPLEX_UNIT_SP, 17);
                    ((EditText) comp.getUiComponent()).setTypeface(externalFont);
                    ((EditText) comp.getUiComponent()).setTypeface(((EditText) comp.getUiComponent()).getTypeface(), Typeface.BOLD);
                    ((EditText) comp.getUiComponent()).setTextColor(getResources().getColor(R.color.edit_color));
                    Drawable drawable = comp.getUiComponent().getBackground(); // get current EditText drawable
//                    drawable.setColorFilter(getResources().getColor(R.color.edit_line), PorterDuff.Mode.SRC_ATOP); // change the drawable color
                    if (Build.VERSION.SDK_INT > 16) {
                        comp.getUiComponent().setBackground(drawable); // set the new drawable to EditText
                    } else {
                        comp.getUiComponent().setBackgroundDrawable(drawable); // use setBackgroundDrawable because setBackground required API 16
                    }
                }
                if (comp.getUiComponent() instanceof Spinner) {
                    comp.getUiComponent().setMinimumHeight(iMinHeight);
                }
                if (comp.getUiComponent() instanceof ImageButton) {
                    if (comp.getGroup().equals("BTScan_PX100")) {
                        comp.getUiComponent().setVisibility(View.GONE);
                    }
                }
            }
        } catch (Exception ex) {
            WmsUtil.showLog(ex);
        }
    }

    public static boolean checkNetwork(Context cont) {
        ConnectivityManager connMgr = (ConnectivityManager) cont.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            Log.v("NetworkInfo", "Connected State");
        }
        boolean connected = (connMgr.getActiveNetworkInfo() != null &&
                connMgr.getActiveNetworkInfo().isAvailable() &&
                connMgr.getActiveNetworkInfo().isConnected());
        return networkInfo != null && networkInfo.isConnected() && networkInfo.isAvailable();
    }

    protected void showMessage(int msgType) {
        CommonEntity entity = this.getCommonEntity();
        if (entity == null) {
            return;
        }
        showMessageDialog(msgType);
    }

    protected String getClassN() {
        return this.getClass().getSimpleName();
    }

    public void showMessageString() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                CommonEntity entity = BaseActivity.this.getCommonEntity();
                if (entity == null) {
                    return;
                }
                Boolean check = false;
                TextView txtError = findViewById(R.id.txtError);
                txtError.setBackgroundColor(getResources().getColor(R.color.main_background));
                txtError.setGravity(Gravity.CENTER_VERTICAL);
                txtError.setTextColor(getResources().getColor(R.color.error_color));
                txtError.setMinHeight(iMinHeight);
                txtError.setPadding(10, 0, 0, 0);
                txtError.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
                txtError.setTypeface(externalFont);
                txtError.setTypeface(txtError.getTypeface(), Typeface.BOLD);
                txtError.setVisibility(View.VISIBLE);

                if (DataBundle.ret_code != -1) {
                    Integer t = DataBundle.ret_code;
                    String className = getClassN();
                    switch (t) {
                        case ErrorCode.ERR_UNKNOW:
                            getCommonEntity().setMsgCode("MS100100");
                            break;
                        case ErrorCode.ERR_SERVICE:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100097;
                                mes.MSGKBN = "1";
                                mes.MSG = "Không có dữ liệu trả về";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100097", mes);
                            }
                            getCommonEntity().setMsgCode("MS100097");
                            break;
                        case ErrorCode.ERR_SERVICE_EXEC:
                            check = true;
                            String myString = getCommonEntity().getMsgContent();
                            if (TextUtils.isEmpty(myString)) {
                                myString = "Error database";
                            }
                            txtError.setText(myString);
                            if (!className.equals("SHCM0010")) {
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                }, 2500);
                            }
                            break;
                        case ErrorCode.ERR_ID:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Vui lòng nhập ID";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100001", mes);
                            }
                            getCommonEntity().setMsgCode("MS100001");
                            break;
                        case ErrorCode.ERR_PASS:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Vui lòng nhập password";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100002", mes);
                            }
                            getCommonEntity().setMsgCode("MS100002");
                            break;
                        case ErrorCode.ERR_IDLENGTH:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Vượt quá 8 kí tự";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100004", mes);
                            }
                            getCommonEntity().setMsgCode("MS100004");
                            break;
                        case ErrorCode.ERR_PASSLENGTH:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Vượt quá 16 kí tự";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100004", mes);
                            }
                            getCommonEntity().setMsgCode("MS100004");
                            break;
                        case ErrorCode.FAIL_LOGIN:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Sai thông tin đăng nhập";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100003", mes);
                            }
                            getCommonEntity().setMsgCode("MS100003");
                            break;
                        case ErrorCode.FAIL_NETWORK:
                            if (className.equals("SHCM0010")) {
                                MessageDto mes = new MessageDto();
                                mes.MSGCD = MessageCode.MS100100;
                                mes.MSGKBN = "1";
                                mes.MSG = "Không có Internet";
                                txtError.setBackgroundColor(getResources().getColor(R.color.login_background));
                                FreshResource.mapMessage.put("MS100109", mes);
                            }
                            getCommonEntity().setMsgCode("MS100109");
                            break;
                        case ErrorCode.ERR_UNKNOW_DEVICE:
                            getCommonEntity().setMsgCode("MS100005");
                            break;

                    }
                }
            }
        });
    }


    private void showMessageDialog(int msgType) {
        String msg = WmsUtil.getMessage(getCommonEntity());
        switch (msgType) {
            case MessageType.MSG_INFO:
            case MessageType.MSG_NOTIFICATION:
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                break;
            case MessageType.MSG_WARNING:
            case MessageType.MSG_CONFIRM:
                new AlertDialog.Builder(BaseActivity.this).setTitle("Warning").setMessage(msg)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        }).setPositiveButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                break;
            case MessageType.MSG_ERROR:
                new AlertDialog.Builder(BaseActivity.this).setTitle("Warning").setMessage(msg)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).setCancelable(true).show();
                break;
            default:
                break;
        }
    }

    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    protected abstract CommonEntity getCommonEntity();

    protected abstract List<ManagedComponent> setupControlList();

    public final void switchScreen(final Class screen, Serializable params) {
        try {
            Intent actFunc = new Intent(getApplicationContext(), screen);
            actFunc.putExtra("data", params);
            startActivity(actFunc);
        } catch (Exception ex) {
            WmsUtil.showLog(ex);
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");

        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        Log.e("DEVICE", capitalize(manufacturer) + " " + model);
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }
}
