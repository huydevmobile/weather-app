package vn.co.brycenvn.weather.entity.cm;

/**
 * Created by q_huy on 1/19/2018.
 */

public class BranchFromEntity {
    private String fromBranchCode;
    private String fromBranchName;
    private String fromBranchrName;

    public BranchFromEntity(String fromBranchCode, String fromBranchName, String fromBranchrName) {
        this.fromBranchCode = fromBranchCode;
        this.fromBranchName = fromBranchName;
        this.fromBranchrName = fromBranchrName;
    }

    public String getFromBranchCode() {
        return fromBranchCode;
    }

    public void setFromBranchCode(String fromBranchCode) {
        this.fromBranchCode = fromBranchCode;
    }

    public String getFromBranchName() {
        return fromBranchName;
    }

    public void setFromBranchName(String fromBranchName) {
        this.fromBranchName = fromBranchName;
    }

    public String getFromBranchrName() {
        return fromBranchrName;
    }

    public void setFromBranchrName(String fromBranchrName) {
        this.fromBranchrName = fromBranchrName;
    }


}
