package vn.co.brycenvn.weather.common.constant;

import java.text.DateFormatSymbols;
import java.util.Locale;

/**
 * @author q_huy
 */
public class FHConst {
    public static final int DEVICE_TYPE = 2;
    // convert from FHConst.cs in C# project
    public static final int CNST_MAXCNT = 20;
    public static final int CNST_MAXT_LENGT_ITEM = 30;
    public static final int CNST_MAXT_LENGT_SIPLNNO = 30;
    public static final int CNST_MAX_LENGTH_SERIAL = 20;
    public static final String EMPTY_STRING = " ";
    public static final String[] WEEK_DAYS = DateFormatSymbols.getInstance(Locale.US).getShortWeekdays();
    public static final String[] MONTH_NAME = DateFormatSymbols.getInstance(Locale.US).getShortMonths();
    public static final String STR_FORMAT_DFLTDATE = "yyyyMMdd";
    public static final String STR_FORMAT_MYSQLDATE = "yyyy-MM-dd";
    // ---------------------------------------------------------------
    // System constants
    // ---------------------------------------------------------------
    public static final String CNST_SYS_DFLTCSTMCD = "BRC00001";
    public static final String CNST_SYS_ERROR = "Error has occured, please contact your system administrator";
    public static final String CNST_SYS_CONNERROR = "Connection lost";
    public static final char CNST_SYS_DELIM = '*'; // default delimiter


    public static final String SERVICE_URL = "service_url";
    public static final String HOME_URL = "home_url";
    public static final String RETURN_CODE = "ret_code";
    public static final String BASE_URL_IMG = "static/img/weather/png/";
    public static final String IMG_TYPE = ".png";
    // Temp text for login
    public static final String USERNAME = "Username";
    public static final String PASSWORD = "Password";
    public static final String BRANCH = "Branch";
    public static final String CUSTOMER = "Customer";
    public static final String TITLE = "Login";
    public static final String KIND_DEVICE = "H27";
    public static final String MS000099 = "MS000099";// Lỗi hệ thống
    public static final String MS000098 = "MS000098";// Lỗi kết nối - lỗi API
    public static final String MS0000101 = "MS00097";// Lỗi không thể kết nối database- có mạng nhưng ko vô đc server
    public static final String MS0000102 = "MS0000102";// Lỗi

}
