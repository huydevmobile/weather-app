package vn.co.brycenvn.weather.common.resourcemng;

import java.util.List;

import vn.co.brycenvn.weather.common.resourcemng.dto.ResourceDto;

interface AppResource {

    /**
     * Lấy text cho một đối tượng
     *
     * @param screenId Tên màn hình
     * @param seqNo    Số hiệu đại diện cho đối tượng
     * @return Text của đối tượng, nếu không tìm thấy thì trả về số hiệu đại diện.
     */
    String getText(String screenId, int seqNo);

    /**
     * Lấy danh sách toàn bộ dữ liệu text màn hình
     *
     * @return
     */
    List<ResourceDto> getTexts();

    /**
     * Lấy nội dung thông báo theo mã thông báo truyền vào.
     *
     * @param msgCd Mã thông báo
     * @return Trả về nội dung thông báo, đây là nội dung thô, có thể vẫn còn các kí hiệu đại diện.
     * Nếu không tìm thấy thì trả về mã thông báo.
     */
    String getMessage(String msgCd);

    /**
     * Lấy danh sách toàn bộ dữ liệu message
     *
     * @return
     */
    List<ResourceDto> getMessages();

    /**
     * Cho biết dữ liệu resource có hiện hữu không
     *
     * @return
     */
    boolean isAvailable();

    /**
     * Nạp dữ liệu resource theo mã truyền vào
     *
     * @param cstmCd Mã khách hàng
     */
    void load(String cstmCd);

    String getMessageKbn(String msgCd);
}
