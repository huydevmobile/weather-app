package vn.co.brycenvn.weather;

import android.app.Activity;
import android.app.Application;

import androidx.room.Room;

import vn.co.brycenvn.weather.common.constant.FHConst;
import vn.co.brycenvn.weather.common.resourcemng.ResourceConfig;
import vn.co.brycenvn.weather.common.resourcemng.ResourceManager;
import vn.co.brycenvn.weather.config.ConfigurationManager;
import vn.co.brycenvn.weather.entity.cm.CommonEntity;
import vn.co.brycenvn.weather.localdb.AppDatabase;

import static vn.co.brycenvn.weather.activity.BaseActivity.checkNetwork;

/**
 * Lớp ứng dụng, chịu trách nhiệm khởi tạo trạng thái ban đầu của h�? thống.
 *
 * @author q_huy
 */
public class ForecastApplication extends Application{
    private static final String DIR_RES_CACHE = "/res_cache";
    public static String kindDevice;
    public static boolean checkH27;
    private static CommonEntity rootCommonEntity;
    public static CommonEntity getRootCommonEntity() {
        return rootCommonEntity;
    }
    private AppDatabase db;
    @Override
    public void onCreate() {
        super.onCreate();
        initSystem();
        db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class, "mydb").build();
//        if (checkNetwork(this)){
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    db.clearAllTables();
//                }
//            }).start();
//        }

    }

    public AppDatabase getDatabase() {
        return db;
    }

    /**
     * Khởi tạo h�? thống
     */
    public void initSystem() {
        // khởi tạo đối tượng common entity, sẽ sử dụng cho to�?n b�? ứng dụng.
        rootCommonEntity = new CommonEntity();
        rootCommonEntity.setCustomerCode(FHConst.CNST_SYS_DFLTCSTMCD);

        // load configuration manager
        ConfigurationManager.load(this);
        ResourceManager.getInstance().config(getRessoureConfig());
        ResourceManager.getInstance().loadResource(FHConst.CNST_SYS_DFLTCSTMCD);


    }

    /**
     * Kiểm tra kết nối mạng.
     * Sử dụng tạm thời trong quá trình phát triển.
     */

    private ResourceConfig getRessoureConfig() {
        // Thiết lập thông tin cấu hình cho resource
        ResourceConfig config = new ResourceConfig();
        config.setLocaLocation(
                getFilesDir().getAbsolutePath() + DIR_RES_CACHE);    // cần cho việc lưu v�? truy xuất cache resource
        config.setRemoteLocation("");                                // hiện tại, không cần cấu hình cho remote resource
        return config;
    }
    private Activity mCurrentActivity = null;
    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }
}
