package vn.co.brycenvn.weather.service.dto.common;

import vn.co.brycenvn.weather.service.dto.AbstractDto;

public class AccessInfoDto extends AbstractDto {


    public String USRCD;


    public String USRNM;


    public String CSTMCD;


    public String CSTMNM;


    public String BRNCHCD;


    public String BRNCHNM;


    public String LANG;


    public String TOKEN;


}
