package vn.co.brycenvn.weather.common.resourcemng;

import java.io.File;
import java.io.FileNotFoundException;

import android.util.Log;

import vn.co.brycenvn.weather.service.CallbackListener;

/**
 * Lớp quản lý resource (text và message), cho phép truy xuất
 *
 * @author q_huy
 */
public class ResourceManager {
    private static ResourceManager instance;
    private static boolean resourceLoaded = false;

    private CacheResource resCache;
    private AppResource currentResource;
    private ResourceConfig config;
    /**
     * Đối tượng xử lí kết quả trả về sau khi lấy việc text từ web service hoàn
     * tất.
     */
    CallbackListener<Object> remoteResourceLoadingCallback = new CallbackListener<Object>() {

        @Override
        public void onSuccess(Object data) {
            updateCache();

        }

        @Override
        public void onFailure(Object data) {
            // TODO Auto-generated method stub
        }
    };

    /**
     * Cập nhật dữ liệu cache (text và message) bằng dữ liệu mới nhất lấy được
     * từ service.
     */
    public static void updateCache() {
        if (instance.config == null) {
            return;
        }
        File cacheFolder = new File(instance.config.getLocaLocation());
        if (!cacheFolder.exists())
        {
            cacheFolder.mkdir();
        }
        try {
            if (instance.currentResource instanceof FreshResource) {
                instance.resCache.save(instance.currentResource.getTexts());
                instance.resCache.save(instance.currentResource.getMessages());
            }
        } catch (FileNotFoundException e) {
            Log.e(ResourceManager.class.getName(), e.getMessage(), e);
        }
    }

    /**
     * Delete cache resource
     *
     * @return Return true if directory is empty or all file is deleted
     * successfully. Return false if as least one file cannot be
     * deleted.
     */
    public static boolean deleteCache() {
        boolean result = true;
        if (instance.config == null) {
            return false;
        }
        File dir = new File(instance.config.getLocaLocation());
        File[] resFiles = dir.listFiles();
        if (resFiles == null) {
            return true;
        }
        for (File f : resFiles) {
            result = result && f.delete();
        }
        return result;
    }

    public static ResourceManager getInstance() {
        if (instance == null) {
            instance = new ResourceManager();
        }
        return instance;
    }

    /**
     * Lấy text cho một đối tượng trên màn hình
     *
     * @param screenId Tên màn hình
     * @param seqNo    Số hiệu đại diện cho phần tử
     * @return
     */
    public static String getText(String screenId, int seqNo) {
        return instance.currentResource.getText(screenId, seqNo);
    }

    /**
     * Lấy nội dung thông báo theo mã truyền vào
     *
     * @param msgCd Mã thông báo.
     * @return Nội dung thông báo, có thể có kí hiệu đại diện.
     */
    public static String getMessage(String msgCd) {
        return instance.currentResource.getMessage(msgCd);
    }

    /**
     * Thiết lập cấu hình cho resource
     *
     * @param config
     */
    public void config(ResourceConfig config) {
        this.config = config;
    }

    /**
     * Nạp dữ liệu resource (text và message) theo mã khách hàng truyền vào. Ban
     * đầu phương thức sẽ tìm dữ liệu cache trên thiết bị, nếu có thì sử dụng dữ
     * liệu từ cache. Nếu chưa tồn tại cache thì tiến hành lấy dữ liệu từ server
     * và lưu vào cache.
     *
     * @param cstmCd Mã khách hàng
     */
    public void loadResource(String cstmCd) {
        FreshResource resFresh;

        // if (!resourceLoaded) {
        if (resCache == null) {
            resCache = new CacheResource(this.config);
        }
        // if (!resCache.isAvailable()) {
        resFresh = new FreshResource();
        // thiết lập xử lí sau khi truy vấn hoàn tất
        resFresh.setLoadingCallback(remoteResourceLoadingCallback);
        resFresh.load(cstmCd);
        currentResource = resFresh;
        resourceLoaded = true;
    }
    public static String getMessageKbn(String msgCd) {
        return instance.currentResource.getMessageKbn(msgCd);
    }

}
