package vn.co.brycenvn.weather.common.constant;

public class ErrorCode {
    public static final int SUCCESS = 0;
    public static final int SUCCESS_OFFLINE = 10;
    public static final int FAIL = -1;

    // business error flag
    public static final int ERR_FLAG1 = 1;
    public static final int ERR_FLAG2 = 2;
    public static final int ERR_FLAG3 = 3;
    public static final int ERR_FLAG4 = 4;
    public static final int ERR_FLAG5 = 5;
    public static final int ERR_FLAG6 = 6;
    public static final int ERR_FLAG7 = 7;
    public static final int ERR_FLAG8 = 8;
    public static final int ERR_FLAG9 = 9;

    // system error
    public static final int ERR_SERVICE = 30;
    public static final int ERR_SERVICE_EXEC = 31;
    public static final int ERR_UNKNOW = 99;
    public static final int FAIL_LOGIN = 100;
    public static final int FAIL_NETWORK = 101;
    public static final int FAIL_DATABASE = 102;
    public static final int ERR_UNKNOW_DEVICE = 98;
    public static final int ERR_NO_DEVICE_PX100 = 97;
    public static final int ERR_HAVE_DEVICE_PX100 = 96;

    public static final int ERR_ID = 95;
    public static final int ERR_PASS = 94;
    public static final int ERR_IDLENGTH = 93;
    public static final int ERR_PASSLENGTH = 92;
}
