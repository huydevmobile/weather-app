package vn.co.brycenvn.weather.common.constant;

public interface MessageType {
    int MSG_NONE = 0;
    int MSG_ERROR = 1;
    int MSG_WARNING = 2;
    int MSG_WARNING_SERIAL = 16;
    int MSG_CONFIRM = 3;
    int MSG_CONFIRM_LIMIT_DATE = 4;
    int MSG_CONFIRM_INPUT_PRODUCT = 8;
    int MSG_INFO = 4;
    int MSG_NOTIFICATION = 5;
    int MSG_NOTIFICATION_CONFIRM_TIME = 6;
    int MSG_NOTIFICATION_TIME_ERROR = 11;
    int MSG_NOTIFICATION_INPUT_PO = 7;
    int MSG_NOTIFICATION_PO_ERROR = 9;
    int MSG_NOTIFICATION_PO_CONFIRM = 12;
    int MSG_NOTIFICATION_BACK = 10;
    int MSG_EMPTY = 13;
    int MSG_PRODUCT_ERROR = 14;
    int MSG_QUANTITY_OVER = 15;
    int MSG_NOT_EXISTS_SERIAL = 99;
}
