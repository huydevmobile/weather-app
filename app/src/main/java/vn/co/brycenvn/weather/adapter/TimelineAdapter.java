package vn.co.brycenvn.weather.adapter;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import vn.co.brycenvn.weather.R;
import vn.co.brycenvn.weather.activity.WeatherActivity;
import vn.co.brycenvn.weather.common.util.WmsUtil;

import static vn.co.brycenvn.weather.activity.WeatherActivity.screenWidth;
import static vn.co.brycenvn.weather.common.constant.FHConst.MONTH_NAME;
import static vn.co.brycenvn.weather.common.constant.FHConst.WEEK_DAYS;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {
    private static final String TAG = "TimelineAdapter";
    public int lastItemPosition = -1;
    public int flagScroll = -1;
    private Calendar calendar = Calendar.getInstance();
    private TimelineView timelineView;
    private Date[] deactivatedDates;
    private OnDateSelectedListener listener;
    private View selectedView;
    public WeatherActivity weatherActivity;

    public int getSelectedPosition() {
        return selectedPosition;
    }

    private int selectedPosition;
    private int NUMBERS_OF_ITEM_TO_DISPLAY = 7, save_postion;
    private boolean isInit;

    public TimelineAdapter(TimelineView timelineView, int selectedPosition) {
        this.timelineView = timelineView;
        this.selectedPosition = selectedPosition;
        isInit = true;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.timeline_item_layout, parent, false);
        return new TimelineAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        resetCalendar();
        calendar.add(Calendar.DAY_OF_YEAR, position);

        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        final boolean isDisabled = holder.bind(month, day, dayOfWeek, year, position);
        if ((isInit && position == 7)) {
            isInit = false;
            highlightItem(holder.rootView, position, year, month, day, dayOfWeek);
        }
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                weatherActivity.callGetData(year, month + 1, day, true);
                if (!isDisabled) {
                    highlightItem(v, position, year, month, day, dayOfWeek);
                } else {
                    if (listener != null)
                        listener.onDisabledDateSelected(year, month, day, dayOfWeek, isDisabled);
                }
            }
        });
        if (position > lastItemPosition) {
            // Scrolled right
            flagScroll = 1;
        } else {
            // Scrolled left
            flagScroll = 0;
        }
        lastItemPosition = position;
    }

    private void highlightItem(View v, int position, int year, int month, int day, int dayOfWeek) {
        unhighlightItem();
        v.setBackground(timelineView.getResources().getDrawable(R.drawable.background_shape));
        selectedPosition = position;
        selectedView = v;
        if (listener != null) listener.onDateSelected(year, month, day, dayOfWeek);
    }

    private void unhighlightItem() {
        for (int childCount = timelineView.getChildCount(), i = 0; i < childCount; ++i) {
            ViewHolder holder = (ViewHolder) timelineView.getChildViewHolder(timelineView.getChildAt(i));
            holder.rootView.setBackground(timelineView.getResources().getDrawable(R.color.maincolor));
        }
    }

    public void highlightItem(View v, int position) {
        unhighlightItem();
        v.setBackground(timelineView.getResources().getDrawable(R.drawable.background_shape));
        selectedPosition = position;
        selectedView = v;
    }

    private void resetCalendar() {
        calendar.set(timelineView.getYear(), timelineView.getMonth(), timelineView.getDate(),
                1, 0, 0);
    }

    /**
     * Set the position of selected date
     *
     * @param selectedPosition active date Position
     */
    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public int getItemCount() {
        return 21;
    }

    public void disableDates(Date[] dates) {
        this.deactivatedDates = dates;
        notifyDataSetChanged();
    }

    public void setDateSelectedListener(OnDateSelectedListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView monthView, dateView, dayView;
        public View rootView;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            monthView = itemView.findViewById(R.id.monthView);
            dateView = itemView.findViewById(R.id.dateView);
            dayView = itemView.findViewById(R.id.dayView);
            rootView = itemView.findViewById(R.id.rootView);
        }

        boolean bind(int month, int day, int dayOfWeek, int year, int position) {
            rootView.getLayoutParams().width = (int) (screenWidth / NUMBERS_OF_ITEM_TO_DISPLAY);
            monthView.setTextColor(timelineView.getMonthTextColor());
            dateView.setTextColor(timelineView.getDateTextColor());
            dayView.setTextColor(timelineView.getDayTextColor());

            if (WmsUtil.isToday(calendar.getTime())) {
                dayView.setText(R.string.today);
            } else {
                dayView.setText(WEEK_DAYS[dayOfWeek]);
            }
            monthView.setText(MONTH_NAME[month].toUpperCase(Locale.US));
            dateView.setText(String.valueOf(month + 1) + "/" + (day));
            monthView.setVisibility(View.INVISIBLE);
            if (selectedPosition == position) {
                rootView.setBackground(timelineView.getResources().getDrawable(R.drawable.background_shape));
                selectedView = rootView;
                int slpos = position;
                new Handler().postDelayed(() -> {
                    if (save_postion != slpos) {
                        save_postion = slpos;
                        selectedView.performClick();
                    }
                }, 500);
            } else {
                rootView.setBackground(timelineView.getResources().getDrawable(R.color.maincolor));
            }

            for (Date date : deactivatedDates) {
                Calendar tempCalendar = Calendar.getInstance();
                tempCalendar.setTime(date);
                if (tempCalendar.get(Calendar.DAY_OF_MONTH) == day &&
                        tempCalendar.get(Calendar.MONTH) == month &&
                        tempCalendar.get(Calendar.YEAR) == year) {
                    monthView.setTextColor(timelineView.getDisabledDateColor());
                    dateView.setTextColor(timelineView.getDisabledDateColor());
                    dayView.setTextColor(timelineView.getDisabledDateColor());

                    rootView.setBackground(timelineView.getResources().getDrawable(R.color.maincolor));
                    return true;
                }
            }
            return false;
        }
    }

    public void setWeatherActivity(WeatherActivity weatherActivity) {
        this.weatherActivity = weatherActivity;
    }
}