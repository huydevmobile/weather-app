package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

public class QualityEntity implements WmsEntity {
    private String qualityCode;
    private String qualityName;
    private String qualityrName;
    private String defaultQltyCode;
    private String defaultQltyName;
    private String defaultQltyrName;

    /**
     *
     */
    public QualityEntity() {
        super();
    }

    /**
     * @param qualityCode
     * @param qualityName
     * @param qualityrName
     */
    public QualityEntity(String qualityCode, String qualityName, String qualityrName) {
        super();
        this.qualityCode = qualityCode;
        this.qualityName = qualityName;
        this.qualityrName = qualityrName;
    }

    public String getQualityCode() {
        return qualityCode;
    }

    public void setQualityCode(String qualityCode) {
        this.qualityCode = qualityCode;
    }

    public String getQualityName() {
        return qualityName;
    }

    public void setQualityName(String qualityName) {
        this.qualityName = qualityName;
    }

    public String getQualityrName() {
        return qualityrName;
    }

    public void setQualityrName(String qualityrName) {
        this.qualityrName = qualityrName;
    }

    public String getDefaultQltyCode() {
        return defaultQltyCode;
    }

    public void setDefaultQltyCode(String defaultQltyCode) {
        this.defaultQltyCode = defaultQltyCode;
    }

    public String getDefaultQltyName() {
        return defaultQltyName;
    }

    public void setDefaultQltyName(String defaultQltyName) {
        this.defaultQltyName = defaultQltyName;
    }

    public String getDefaultQltyrName() {
        return defaultQltyrName;
    }

    public void setDefaultQltyrName(String defaultQltyrName) {
        this.defaultQltyrName = defaultQltyrName;
    }

}
