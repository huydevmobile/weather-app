package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

public class ReasonEntity implements WmsEntity {
    private String reasonCode;
    private String reasonName;
    private String reasonrName;
//    private String defaultQltyCode;
//    private String defaultQltyName;
//    private String defaultQltyrName;

    /**
     *
     */
    public ReasonEntity() {
        super();
    }

    /**
     * @param reasonCode
     * @param reasonName
     * @param reasonrName
     */
    public ReasonEntity(String reasonCode, String reasonName, String reasonrName) {
        this.reasonCode = reasonCode;
        this.reasonName = reasonName;
        this.reasonrName = reasonrName;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public String getReasonrName() {
        return reasonrName;
    }

    public void setReasonrName(String reasonrName) {
        this.reasonrName = reasonrName;
    }
}
