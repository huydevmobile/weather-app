package vn.co.brycenvn.weather.service;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.widget.ImageButton;

/**
 * Giao diện cho phép thực hiện gọi các service và nhận về kết quả
 *
 * @author q_huy
 */
public interface ServiceCallable<E extends Object> {
    void get(Request rq, CallbackListener<E> callback);

    void post(Context context, Request rq, CallbackListener<E> callback);
    void post(Activity context, boolean showLoading, Request rq, CallbackListener<E> callback, BroadcastReceiver mbroad, ImageButton btn);
    void get(Activity context, boolean showLoading, Request rq, CallbackListener<E> callback, BroadcastReceiver mbroad, ImageButton btn,boolean online);
    void put(Request rq, CallbackListener<E> callback);
    void pop(Request rq, CallbackListener<E> callback);
    void delete(Request rq, CallbackListener<E> callback);

}
