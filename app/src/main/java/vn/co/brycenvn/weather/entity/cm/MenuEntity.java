package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

/**
 * Menu item define
 *
 * @author q_huy
 */
public class MenuEntity implements WmsEntity {
    private String menuCode;
    private String menuName;
    private String menuShortName;
    private String menuDispNo;
    private String functionId;


    /**
     * @param menuCode
     * @param menuName
     * @param menuShortName
     * @param menuDispNo
     * @param functionId
     */
    public MenuEntity(String menuCode, String menuName, String menuShortName, String menuDispNo, String functionId) {
        super();
        this.menuCode = menuCode;
        this.menuName = menuName;
        this.menuShortName = menuShortName;
        this.menuDispNo = menuDispNo;
        this.functionId = functionId;
    }

    public String getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(String menuCode) {
        this.menuCode = menuCode;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuShortName() {
        return menuShortName;
    }

    public void setMenuShortName(String menuShortName) {
        this.menuShortName = menuShortName;
    }

    public String getMenuDispNo() {
        return menuDispNo;
    }

    public void setMenuDispNo(String menuDispNo) {
        this.menuDispNo = menuDispNo;
    }

    public String getFunctionId() {
        return functionId;
    }

    public void setFunctionId(String functionId) {
        this.functionId = functionId;
    }
}
