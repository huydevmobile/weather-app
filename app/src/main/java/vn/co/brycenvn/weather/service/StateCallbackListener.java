package vn.co.brycenvn.weather.service;

import vn.co.brycenvn.weather.common.constant.ErrorCode;

public abstract class StateCallbackListener<E extends Object> implements CallbackListener<E> {
    protected int executionState = ErrorCode.SUCCESS;

    protected int executionState2 = ErrorCode.SUCCESS;

    /**
     * Trả về trạng thái của lần thực thi cuối cùng.
     *
     * @return
     */
    public final int getState() {
        return this.executionState;
    }

}
