package vn.co.brycenvn.weather.service;

public interface CallbackListener<E extends Object> {
    //void callback(Object data);
    //void onSuccess(Object data);

    /**
     * Hàm gọi lại khi xử lí thành công
     *
     * @param data Dữ liệu trả về của xử lí
     * @throws Exception
     */

    void onSuccess(E data) throws Exception;

    /**
     * Hàm gọi lại khi xử lí thất bại
     *
     * @param data Dữ liệu trả về của xử lí
     * @throws Exception
     */

    void onFailure(E data) throws Exception;
    //int getState();
}
