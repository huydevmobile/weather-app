package vn.co.brycenvn.weather.common.resourcemng.dto;


public class MessageDto extends ResourceDto {
    public static final String DTO_ID = "shcmMsgRows";
    public String MSGCD;
    public String MSG;
    public String MSGKBN;
    public String BTNPSTN;

    public MessageDto() {
    }

    /**
     * @param mSGCD
     * @param mSG
     * @param mSGKBN
     * @param bTNPSTN
     */
    public MessageDto(String mSGCD, String mSG, String mSGKBN, String bTNPSTN) {
        super();
        MSGCD = mSGCD;
        MSG = mSG;
        MSGKBN = mSGKBN;
        BTNPSTN = bTNPSTN;
    }

    public MessageDto(String[] arr) {
        if (arr.length > 0) {
            MSGCD = arr[0];
        }
        if (arr.length > 1) {
            MSG = arr[1];
        }
    }


    @Override
    public String toString() {
        return MSGCD + "\t" + MSG;
    }
}
