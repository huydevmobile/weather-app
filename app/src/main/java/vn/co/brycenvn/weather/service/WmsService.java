package vn.co.brycenvn.weather.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ImageButton;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import vn.co.brycenvn.weather.activity.BaseActivity;
import vn.co.brycenvn.weather.common.managed.ManagedComponent;
import vn.co.brycenvn.weather.entity.cm.CommonEntity;

/**
 * Lớp triển khai giao diện ServiceCallable, cho phép thực hiện việc gọi đến các
 * service API. Hiện tại chỉ triển khai phương thức POST.
 *
 * @author q_huy
 */

public class WmsService extends BaseActivity implements ServiceCallable {
    /**
     * Trust every server - dont check for any certificate
     */
    public enum RequestMethod {
        GET,
        HEAD,
        POST,
        PUT,
        PATCH,
        DELETE,
        OPTIONS,
        TRACE;
    }

    private static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");
    private String serviceUrl;
    private Gson gSon;
    public static ProgressDialog pDialog;

    public WmsService(String url) {
        this.serviceUrl = url;
        gSon = new Gson();
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

            OkHttpClient okHttpClient = builder
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .build();
//            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void get(Request request, CallbackListener callback) {

    }

    @Override
    public void put(Request request, CallbackListener callback) {
        // TODO Auto-generated method stub

    }

    @Override
    public void pop(Request request, CallbackListener callback) {
        // TODO Auto-generated method stub

    }

    @Override
    public void delete(Request request, CallbackListener callback) {
        // TODO Auto-generated method stub
    }

    String getServiceUrl() {
        return serviceUrl;
    }

    @Override
    public void post(final Activity context, boolean showLoading, final Request request, final CallbackListener callback, BroadcastReceiver mBroadcastReceiver, ImageButton btnNext) {
        if (context != null) {
            if (onRequest) {
                setDismissDialog();
                return;
            }
            if (checkNetwork(context)) {
                if (showLoading && !onRequest) {
                    pDialog = ProgressDialog.show(context, "", "Loading..", true);
                    pDialog.setCanceledOnTouchOutside(false);
                }
                if (context != null)
                    if (btnNext != null)
                        btnNext.setEnabled(false);
                service(request, callback, context, btnNext, RequestMethod.POST,false);
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject jsonObj = new JSONObject("{\"Error\":\"NoInternet\"}");
                            onRequest = false;
                            setDismissDialog();
                            callback.onFailure(jsonObj);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();

            }
        }
    }

    @Override
    public void get(Activity context, boolean showLoading, Request rq, CallbackListener callback, BroadcastReceiver mbroad, ImageButton btn,boolean online) {
        if (context != null) {
            if (onRequest && online) {
                setDismissDialog();
                return;
            }
            if (checkNetwork(context)) {
                if (showLoading && !onRequest && online) {
                    pDialog = ProgressDialog.show(context, "", "Loading..", true);
                    pDialog.setCanceledOnTouchOutside(false);
                }
                if (context != null)
                    if (btn != null)
                        btn.setEnabled(false);
                service(rq, callback, context, btn, RequestMethod.GET,online);
            } else {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
//                            JSONObject jsonObj = new JSONObject("{\"Error\":\"NoInternet\"}");
                            JSONArray jsonArray = new JSONArray("[{\"Error\":\"NoInternet\"}]");
                            onRequest = false;
                            setDismissDialog();
                            callback.onFailure(jsonArray);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        }
    }

    public static void showDialog(Context context) {
        pDialog = ProgressDialog.show(context, "", "Loading..", true);
        pDialog.setCanceledOnTouchOutside(false);
    }

    public void service(final Request request, final CallbackListener callback, final Context context, final ImageButton btn, RequestMethod requestMethod,boolean online) {
        if (onRequest) return;
        if (online){
            onRequest = true;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                new ServiceCall() {

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                    }

                    protected JSONObject doInBackground(Request[] params) {

                        Request request = params[0];
                        JsonObject jO = null;
                        try {
                            jO = new JsonObject();
                            for (Map.Entry<String, Object> item : request.getParams().entrySet()) {
                                JsonElement element = gSon.toJsonTree(item.getValue());
                                jO.add(item.getKey(), element);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        OkHttpClient client = getUnsafeOkHttpClient();
                        String jSon = jO.toString();
                        String url = request.getRequestUrl();
                        RequestBody body = RequestBody.create(JSON, jSon);
                        System.out.println(new Date().toString() + "[APP-API - Request] " + jSon);
                        okhttp3.Request.Builder builder = new okhttp3.Request.Builder();
                        okhttp3.Request req = null;
                        switch (requestMethod) {
                            case POST:
                                req = builder
                                        .url(url)
                                        .post(body)
                                        .build();
                                break;
                            case GET:
                                req = builder
                                        .url(url)
                                        .get()
                                        .build();
                                break;
                            case PUT:
                                req = builder
                                        .url(url)
                                        .put(body)
                                        .build();
                                break;
                            case PATCH:
                                break;
                            case DELETE:
                                req = builder
                                        .url(url)
                                        .delete(body)
                                        .build();
                                break;
                        }

                        Response response = null;
                        String resStr=null;
                        try {

                            switch (requestMethod) {
                                case POST:
                                    response = client.newCall(req).execute();
                                    resStr = response.body().string();
                                    System.out.println(new Date().toString() + "[API - Response] " + resStr);
                                    break;
                                case GET:
                                    client.newCall(req).enqueue(new Callback() {

                                        @Override
                                        public void onFailure(Call call, IOException e) {
                                            try {
                                                onRequest = false;
                                                setDismissDialog();
                                                callback.onFailure(null);
                                            } catch (Exception ex) {
                                                ex.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onResponse(Call call, Response response) throws IOException {
                                            String resStr = response.body().string();
                                            try {
                                                JSONArray jsonArray = new JSONArray(resStr);
                                            if (context != null)
                                                if (btn != null)
                                                    btn.setEnabled(true);
                                            onRequest = false;
                                            setDismissDialog();
                                                callback.onSuccess(jsonArray);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });
                                    break;
                            }


                            return resStr!=null?new JSONObject(resStr):null;
                        } catch (IOException e) {
                            try {
//                                if (context != null)
//                                    if (btn != null)
//                                        btn.setEnabled(true);
                             /*   if (pDialog != null)
                                    pDialog.dismiss();*/
                                onRequest = false;
                                setDismissDialog();
                                callback.onFailure(null);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        } catch (JSONException e) {
                            try {
                           /*     if (pDialog != null)
                                    pDialog.dismiss();*/
                                onRequest = false;
                                setDismissDialog();
                                callback.onFailure(null);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                        return null;
                    }

                    @Override
                    protected void onCancelled(JSONObject jsonObject) {
                       /* if (pDialog != null)
                            pDialog.dismiss();*/
                        onRequest = false;
                        setDismissDialog();
                        try {
                            if (context != null)
                                if (btn != null)
                                    btn.setEnabled(true);
                            callback.onFailure(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        super.onCancelled(jsonObject);
                    }

                    @Override
                    protected void onCancelled() {
                        /*if (pDialog != null)
                            pDialog.dismiss();*/
                        onRequest = false;
                        setDismissDialog();
                        try {
                            if (context != null)
                                if (btn != null)
                                    btn.setEnabled(true);
                            callback.onFailure(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        super.onCancelled();
                    }

                    @Override
                    protected void onPostExecute(JSONObject result) {
                        try {
                            if (requestMethod==RequestMethod.POST){
                                if (context != null)
                                    if (btn != null)
                                        btn.setEnabled(true);
                           /* if (pDialog != null)
                                pDialog.dismiss();*/
                                onRequest = false;
                                setDismissDialog();
                                if (result!=null){
                                    callback.onSuccess(result);
                                }
                            }
                        } catch (Exception e) {
                            try {
                                callback.onFailure(null);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                            e.printStackTrace();
                        }
                    }
                }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,request);
            }
        }).start();
    }

    public static void setDismissDialog() {
        if (pDialog != null)
            pDialog.dismiss();
    }

    @Override
    public void post(Context context, Request rq, CallbackListener callback) {

    }

    @Override
    protected CommonEntity getCommonEntity() {
        return null;
    }

    @Override
    protected List<ManagedComponent> setupControlList() {
        return null;
    }
}

