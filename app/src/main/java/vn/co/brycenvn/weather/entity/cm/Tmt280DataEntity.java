package vn.co.brycenvn.weather.entity.cm;

import vn.co.brycenvn.weather.entity.WmsEntity;

/**
 * Created by q_huy on 11/23/2017.
 */

public class Tmt280DataEntity implements WmsEntity{
    private   String CSTMCD;
    private   String DIVKBN;
    private   String LOCALDEFLT;

    public Tmt280DataEntity(String CSTMCD, String DIVKBN, String LOCALDEFLT, String FOREIGNNM3) {
        this.CSTMCD = CSTMCD;
        this.DIVKBN = DIVKBN;
        this.LOCALDEFLT = LOCALDEFLT;
        this.FOREIGNNM3 = FOREIGNNM3;
    }

    public String getCSTMCD() {
        return CSTMCD;
    }

    public void setCSTMCD(String CSTMCD) {
        this.CSTMCD = CSTMCD;
    }

    public String getDIVKBN() {
        return DIVKBN;
    }

    public void setDIVKBN(String DIVKBN) {
        this.DIVKBN = DIVKBN;
    }

    public String getLOCALDEFLT() {
        return LOCALDEFLT;
    }

    public void setLOCALDEFLT(String LOCALDEFLT) {
        this.LOCALDEFLT = LOCALDEFLT;
    }

    public String getFOREIGNNM3() {
        return FOREIGNNM3;
    }

    public void setFOREIGNNM3(String FOREIGNNM3) {
        this.FOREIGNNM3 = FOREIGNNM3;
    }

    private   String FOREIGNNM3;

}
