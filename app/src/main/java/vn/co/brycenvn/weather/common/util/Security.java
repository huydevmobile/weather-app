package vn.co.brycenvn.weather.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Security {
    public static String getMD5Hash(String text) {
        if (text == null || text.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(text.getBytes());
            byte[] messageDigest = digest.digest();

            for (byte b : messageDigest) {
                String s = Integer.toHexString(0xFF & b);
                if (s.length() < 2) {
                    s = "0" + s;
                }
                sb.append(s);
            }
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return sb.toString();
    }
}
