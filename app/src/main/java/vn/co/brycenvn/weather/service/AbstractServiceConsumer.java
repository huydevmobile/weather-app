package vn.co.brycenvn.weather.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import vn.co.brycenvn.weather.service.dto.common.ErrorDto;

/**
 * Lớp sử dụng service và xử lí dữ liệu trả về trước khi chuyển đến tầng UI.
 *
 * @author q_huy
 */
public abstract class AbstractServiceConsumer {
    protected Gson gsConverter = new Gson();
    private List<ErrorDto> responseErrors = new ArrayList<>();

    /**
     * Tạo request để gửi tới server
     *
     * @param api Tên API cần gọi
     * @return Đối tượng Request
     */
    protected Request createRequest(String api) {
        Request rq = new Request(getServiceCaller().getServiceUrl() + api);
        return rq;
    }

    /**
     * Get ServiceCallable instance
     *
     * @return
     */
    protected abstract WmsService getServiceCaller();

    /**
     * Trả về False nếu dữ liệu trả về có chứa thông tin lỗi, ngược lại trả về
     * True Hàm này mặc định trả về True nếu dữ liệu truyền vào Null, do vậy cần
     * kiểm tra điều kiện dữ liệu khác Null trước.
     *
     * @param dataOj
     * @return
     */
    protected final boolean checkError(Object dataOj) {
        if (dataOj instanceof JSONObject){
            JSONObject data= (JSONObject) dataOj;
            if (data == null) {
                return true;
            }
            if (!data.has("fatalError") && !data.has("normalError")) {
                return true;
            }
            responseErrors.clear();
            try {
                ErrorDto dtoErr = null;
                JSONArray jsaError = null;
                if (data.has("fatalError")) {
                    jsaError = data.getJSONArray("fatalError");
                } else if (data.has("normalError")) {
                    jsaError = data.getJSONArray("normalError");
                }
                if (jsaError != null && jsaError.length() > 0) {
                    for (int i = 0; i < jsaError.length(); i++) {
                        this.responseErrors.add(gsConverter.fromJson(jsaError.getJSONObject(i).toString(), ErrorDto.class));
                    }
                    return false;
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Nhận về đối tượng thông chứa dữ liệu lỗi trả về sau mỗi lần request. Gọi
     * phương thức này ngay sau khi gọi {@link #checkError(JSONObject)}
     *
     * @return Đối tượng chứa thông tin lỗi
     */
    protected final List<ErrorDto> getResponseErrors() {
        return this.responseErrors;
    }
}
