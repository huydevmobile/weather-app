package vn.co.brycenvn.weather.common.resourcemng;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import vn.co.brycenvn.weather.ForecastApplication;
import vn.co.brycenvn.weather.common.resourcemng.dto.MessageDto;
import vn.co.brycenvn.weather.common.resourcemng.dto.ResourceDto;
import vn.co.brycenvn.weather.common.resourcemng.dto.TextDto;

/**
 * Lớp truy xuất dữ liệu text và message lưu trên thiết bị
 *
 * @author q_huy
 */
public class CacheResource implements AppResource {
    private static final String FILE_NAME_TEXT = "res_text.txt";
    private static final String FILE_NAME_MESSAGE = "res_message.txt";

    private ResourceConfig config;
    private Map<String, TextDto> mapText = new HashMap<String, TextDto>();
    private Map<String, MessageDto> mapMessage = new HashMap<String, MessageDto>();

    public CacheResource(ResourceConfig config) {
        this.config = config;
    }

    @Override
    public String getText(String screenId, int seqNo) {
        TextDto dto = mapText.get(screenId + "_" + seqNo);
        return dto != null ? dto.FORMITEMNM : "%" + seqNo;
    }

    @Override
    public String getMessage(String msgCd) {
        MessageDto dto = mapMessage.get(msgCd);
        return dto != null ? dto.MSG : msgCd;
    }

    @Override
    public List<ResourceDto> getTexts() {
        return new ArrayList<ResourceDto>(mapText.values());
    }

    @Override
    public List<ResourceDto> getMessages() {
        return new ArrayList<ResourceDto>(mapMessage.values());
    }

    @Override
    public void load(String id) {
        loadText(id);
        loadMessage(id);

    }

    @Override
    public String getMessageKbn(String msgCd) {
        return null;
    }

    /**
     * Nạp dữ liệu text từ file cache trên thiết bị
     *
     * @param id Mã ứng với người dùng (CSTMCD)
     * @return
     */
    private boolean loadText(String id) {
        boolean bResult = true;
        File f = new File(config.getLocaLocation(), id + "_" + FILE_NAME_TEXT);
        if (!f.exists()) {
            return false;
        }

        FileReader freader = null;
        BufferedReader reader = null;
        try {
            freader = new FileReader(f);
            reader = new BufferedReader(freader);
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                TextDto dto = new TextDto(tmp.split("\t"));
                mapText.put(dto.SCRNID + "_" + dto.SEQNO, dto);
            }
        } catch (FileNotFoundException e) {
            bResult = false;
            e.printStackTrace();
        } catch (IOException e) {
            bResult = false;
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (freader != null) {
                try {
                    freader.close();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }
        return bResult;
    }

    /**
     * Nạp dữ liệu message từ file cache
     *
     * @param id Mã đại diện người dùng (CSTMCD)
     * @return
     */
    private boolean loadMessage(String id) {
        boolean bResult = true;
        File f = new File(config.getLocaLocation(), id + "_" + FILE_NAME_MESSAGE);
        if (!f.exists()) {
            return false;
        }

        FileReader freader = null;
        BufferedReader reader = null;
        try {
            freader = new FileReader(f);
            reader = new BufferedReader(freader);
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                MessageDto dto = new MessageDto(tmp.split("\t"));
                mapMessage.put(dto.MSGCD, dto);
            }
        } catch (FileNotFoundException e) {
            bResult = false;
            e.printStackTrace();
        } catch (IOException e) {
            bResult = false;
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (freader != null) {
                try {
                    freader.close();
                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }
        return bResult;
    }

    @Override
    public boolean isAvailable() {
        File f = new File(config.getLocaLocation(), ForecastApplication.getRootCommonEntity().getCustomerCode() + "_" + FILE_NAME_TEXT);
        return f.exists();
    }

    /**
     * Lưu dữ liệu xuống file
     *
     * @param res
     * @throws FileNotFoundException
     */
    public void save(List<ResourceDto> res) throws FileNotFoundException {
        if (res == null || res.size() == 0) {
            return;
        }
        ResourceDto dto = res.get(0);
        if (dto instanceof TextDto) {
            saveText(res);
        } else if (dto instanceof MessageDto) {
            saveMessage(res);
        }
    }

    /**
     * Lưu dữ liệu text
     *
     * @param res
     * @throws FileNotFoundException
     */
    private void saveText(List<ResourceDto> res) throws FileNotFoundException {
        PrintWriter writer = null;
        File f = new File(config.getLocaLocation(), ForecastApplication.getRootCommonEntity().getCustomerCode() + "_" + FILE_NAME_TEXT);
        try {
            writer = new PrintWriter(f);
            for (ResourceDto dto : res) {
                writer.println(dto.toString());
            }
        } finally {
            if (writer != null) {
                writer.close();
            }
        }

    }

    /**
     * Lưu dữ liệu message
     *
     * @param res
     * @throws FileNotFoundException
     */
    private void saveMessage(List<ResourceDto> res) throws FileNotFoundException {
        PrintWriter writer = null;
        File f = new File(config.getLocaLocation(),
                ForecastApplication.getRootCommonEntity().getCustomerCode() + "_" + FILE_NAME_MESSAGE);
        try {
            writer = new PrintWriter(f);
            for (ResourceDto dto : res) {
                writer.println(dto.toString());
            }
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

}
