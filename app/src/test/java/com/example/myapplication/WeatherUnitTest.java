package com.example.myapplication;

import org.junit.Test;

import vn.co.brycenvn.weather.activity.WeatherActivity;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class WeatherUnitTest {
    Data data=new Data();
    @Test
    public void checkFindTheMostcCommon() {
        assertEquals("mưa", WeatherActivity.findTheMostcCommon(data.list));
    }

}